import { FetchError } from "./errors";

export type HookWithLoading<T, Error = FetchError> =
  | {
      loading: true;
      result: null;
      error: null;
    }
  | {
      loading: false;
      result: T | null;
      error: Error;
    }
  | {
      loading: false;
      result: T;
      error: null;
    };
