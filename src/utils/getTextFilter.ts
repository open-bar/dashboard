import slugify from "slugify";

type StringGetter<T> = (obj: T) => string;

const slugifyOptions = {
  locale: "fr",
  lower: true,
  trim: true,
};

export function getTextFilter<T>(
  filter: string,
  keys: (keyof T | StringGetter<T>)[]
) {
  const filterWords = slugify(filter, slugifyOptions).split("-");
  return (obj: T) => {
    const objectWords = keys
      .map((key) =>
        slugify(
          typeof key === "function" ? key(obj) : `${obj[key]}`,
          slugifyOptions
        ).split("-")
      )
      .flat();
    return filterWords.every((word) =>
      objectWords.some((objWord) => objWord.indexOf(word) >= 0)
    );
  };
}

export type RankedResult<T> = {
  value: T;
  rank: number;
};

const sum = (a: number, b: number) => a + b;

export function getTextFilterWithRank<T>(
  filter: string,
  keys: (keyof T | StringGetter<T>)[]
): (obj: T) => RankedResult<T> {
  const filterWords = slugify(filter, slugifyOptions).split("-");
  const keyLength = keys.length;
  return (obj: T) => {
    const rank = filterWords
      .map((word) => {
        return keys
          .map((key, keyIndex) => {
            const keyWords = slugify(
              typeof key === "function" ? key(obj) : `${obj[key]}`,
              slugifyOptions
            ).split("-");
            if (keyWords.some((keyWord) => keyWord.indexOf(word) >= 0)) {
              return 1 << (keyLength - keyIndex);
            }
            return 0;
          })
          .reduce(sum, 0);
      })
      .reduce(sum, 0);
    return {
      value: obj,
      rank,
    };
  };
}

export const resultIsRanked = <T>(result: RankedResult<T>): boolean =>
  result.rank > 0;
export const getValueFromRanked = <T>(result: RankedResult<T>): T =>
  result.value;
export const rankedResultSorter = <T>(
  resultA: RankedResult<T>,
  resultB: RankedResult<T>
): number => resultB.rank - resultA.rank;
