import { describe, expect, it } from "vitest";
import { isDefined } from "./isDefined";

describe("isDefined", () => {
  it("should return false if value is null", () => {
    expect(isDefined(null)).toBe(false);
  });
  it("should return false if value is undefined", () => {
    expect(isDefined(undefined)).toBe(false);
  });
  it("should return true if value is an empty string", () => {
    expect(isDefined("")).toBe(true);
  });
  it("should return true if value in 0", () => {
    expect(isDefined(0)).toBe(true);
  });
});
