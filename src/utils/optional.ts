import * as t from "io-ts";

export const optional = <T extends t.Mixed>(
  type: T
): t.UnionC<[T, t.UndefinedC, t.NullC]> => t.union([type, t.undefined, t.null]);
