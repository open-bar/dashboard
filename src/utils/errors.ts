import { PaginationInfoI } from "./pagination";

export enum FetchErrorType {
  HTTP_REQUEST_ERROR = "HTTP_REQUEST_ERROR",
  PARSING_ERROR = "PARSING_ERROR",
  UNAUTHORIZED = "UNAUTHORIZED",
  CUSTOM_ERROR = "CUSTOM_ERROR",
}

export interface ParsingErrorType extends Record<string, unknown> {
  data: unknown;
  errorsKey: string[] | undefined;
}

export interface CustomFetchError {
  type: FetchErrorType.CUSTOM_ERROR;
}

export type GenericFetchError =
  | {
      type: FetchErrorType.UNAUTHORIZED;
      partialResults?: never;
      code?: never;
      error?: string;
    }
  | {
      type: FetchErrorType.HTTP_REQUEST_ERROR;
      partialResults?: never;
      code: number;
      error: string;
    };

export type FetchError =
  | GenericFetchError
  | {
      type: FetchErrorType.PARSING_ERROR;
      error: ParsingErrorType;
    };

export type FetchArrayError<T> =
  | GenericFetchError
  | {
      type: FetchErrorType.PARSING_ERROR;
      paginationInfo?: PaginationInfoI;
      partialResults: T[];
      errors: ParsingErrorType[];
    };

export const genericErrorMessage: Record<FetchErrorType, string> = {
  [FetchErrorType.HTTP_REQUEST_ERROR]: "Une erreur inattendue est servenue.",
  [FetchErrorType.UNAUTHORIZED]:
    "Vous devez être identifié pour effectuer cette action.",
  [FetchErrorType.PARSING_ERROR]: "Une erreur inattendue est servenue.",
  [FetchErrorType.CUSTOM_ERROR]: "Une erreur inattendue est servenue.",
};
