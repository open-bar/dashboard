import { FetchError } from "./errors";
import { HookWithLoading } from "./HookWithLoading";

export const getResultState = <ResultType, ErrorType = FetchError>(
  result: ResultType | null | undefined,
  error: ErrorType | null
): HookWithLoading<ResultType, ErrorType> => {
  if (error) {
    return {
      error,
      loading: false,
      result: result || null,
    };
  } else if (!result) {
    return {
      error: null,
      loading: true,
      result: null,
    };
  }
  return {
    error: null,
    loading: false,
    result,
  };
};
