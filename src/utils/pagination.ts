import * as t from "io-ts";

export const PaginationInfoIO = t.type({
  current_page: t.number,
  per_page: t.number,
  total_items: t.number,
  total_pages: t.number,
});

export type PaginationInfoI = t.TypeOf<typeof PaginationInfoIO>;

export type PaginatedResultsI<T> = PaginationInfoI & {
  results: T[];
};
