interface OpaqueType<OpaqueT> {
  _type: OpaqueT;
}

export type Opaque<T, OpaqueT> = T & OpaqueType<OpaqueT>;

/* usage example:
      export type UserId = Opaque<string, { readonly T: unique symbol }>
  */
