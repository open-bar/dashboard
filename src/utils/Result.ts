export type ResultSuccessType<ResultType> = {
  success: true;
  value: ResultType;
  error?: undefined;
};
export type ResultErrorType<ErrorType = string> = {
  success: false;
  value?: undefined;
  error: ErrorType;
};

export type Result<ResultType, ErrorType = string> =
  | ResultSuccessType<ResultType>
  | ResultErrorType<ErrorType>;

export const isResultSuccess = <ResultType, ErrorType = string>(
  result: Result<ResultType, ErrorType>
): result is ResultSuccessType<ResultType> => result.success;

export const isResultError = <ResultType, ErrorType = string>(
  result: Result<ResultType, ErrorType>
): result is ResultErrorType<ErrorType> => !result.success;

export const ResultSuccess = <ResultType>(
  value: ResultType
): ResultSuccessType<ResultType> => ({
  success: true,
  value,
});
export const ResultError = <ErrorType>(
  error: ErrorType
): ResultErrorType<ErrorType> => ({
  success: false,
  error,
});

export const getResultSuccessValue = <ResultType>(
  result: ResultSuccessType<ResultType>
): ResultType => result.value;

export const getResultErrorValue = <ErrorType>(
  result: ResultErrorType<ErrorType>
): ErrorType => result.error;

export const resultPipe = <ResultType, ErrorType>(
  result: Result<ResultType, ErrorType>,
  success: (value: ResultType) => unknown,
  failure: (error: ErrorType) => unknown
) => {
  if (isResultSuccess(result)) {
    success(result.value);
  } else {
    failure(result.error);
  }
};
