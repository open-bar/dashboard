import {
  fireEvent,
  getByRole,
  getByText,
  render,
  renderHook,
  screen,
  waitFor,
} from "@testing-library/react";
import { Visitor } from "./Visitor";
import { TestApp } from "./test-utils";
import { authState } from "@/domains/identity/authentication/authAtom";
import { mockedRegisteredUser } from "@/domains/identity/IdentityProvider/IdentityProvider.inMemory";
import { assert, expect } from "vitest";
import React from "react";
import { useAtom } from "jotai";
import { AccountingGroupI, ProductInCreationI } from "@/domains/catalog/types";
import { isDefined } from "@/utils/isDefined";
import { isResultSuccess } from "@/utils/Result";
import {
  createVatRate,
  getAmountValue,
  renderVatRate,
} from "@/domains/catalog/utils";

class RegisteredUser extends Visitor {
  constructor() {
    super();
  }

  visisiting(path: string) {
    this.identityProvider
      .login({
        username: mockedRegisteredUser.username,
        password: mockedRegisteredUser.password,
      })
      .then((result) => {
        if (isResultSuccess(result)) {
          this.identityProvider.setToken(result.value.token);
        }
      });
    render(
      this.identityWrapper({
        children: (
          <TestApp
            initialRoute={path}
            store={this.store}
            initialValues={[
              [
                authState,
                {
                  currentUser: mockedRegisteredUser,
                  loginInProgress: false,
                  registrationInProgress: false,
                },
              ],
            ]}
          />
        ),
      })
    );
    const { result: authStateResult } = renderHook(() => useAtom(authState), {
      wrapper: this.wrapper,
    });
    React.act(() => {});
    expect(authStateResult.current[0].currentUser).toEqual(
      mockedRegisteredUser
    );

    return this;
  }

  async currentPageHasTitle(title: string) {
    const mainTitle = await waitFor(() => {
      const titleContainer = screen.getByRole("heading", { level: 1 });
      assert(
        titleContainer.textContent === title,
        `Unable to find page title ${title}`
      );
      return titleContainer;
    });
    expect(mainTitle).toBeDefined();
    expect(mainTitle.textContent).toEqual(title);
    return this;
  }

  async isOnTheAccountingGroupsPage() {
    const mainTitle = await waitFor(() =>
      screen.getByRole("heading", { level: 1 })
    );
    expect(mainTitle).toBeDefined();
    expect(mainTitle.textContent).toEqual("Groupes comptables");
    return this;
  }

  async canViewAccountingGroup(accountingGroup: Omit<AccountingGroupI, "id">) {
    await waitFor(() => screen.getAllByText("Taux de TVA").length === 1);
    await waitFor(() => screen.getByText(accountingGroup.name));
    expect(screen.getByText(accountingGroup.name)).toBeDefined();
    const line = screen.getByText(accountingGroup.name);
    assert(isDefined(line));
    const parent = line.parentElement?.parentElement;
    assert(isDefined(parent));
    expect(
      getByText(
        parent,
        new RegExp(
          `${renderVatRate(accountingGroup.vatRate).split("%")[0].trim()}`,
          "i"
        )
      )
    ).toBeDefined();
    return this;
  }

  async clickOnCreateAccoutingGroup() {
    fireEvent.click(screen.getByText("Ajouter"));
    await waitFor(() => screen.getByLabelText("Nom du groupe comptable"));
    expect(screen.getByLabelText("Nom du groupe comptable")).toBeDefined();
    expect(screen.getByLabelText("Taux de TVA")).toBeDefined();
    const submitButton = screen.getByRole("button", {
      name: /Créer/i,
    });
    React.act(() => {
      fireEvent.change(screen.getByLabelText("Nom du groupe comptable"), {
        target: { value: "test-groupe-comptable-1" },
      });
      fireEvent.change(screen.getByLabelText("Taux de TVA"), {
        target: { value: 10 },
      });
      fireEvent.click(submitButton);
    });
    await this.canViewAccountingGroup({
      vatRate: createVatRate(10),
      name: "test-groupe-comptable-1",
    });

    return this;
  }

  async selectAccountingGroupValue(accountingGroup: AccountingGroupI) {
    const AutoCompleteSearch = screen.getByTestId("select-accounting-group");
    const Input = getByRole(AutoCompleteSearch, "combobox");

    fireEvent.change(Input, { target: { value: accountingGroup.name } });

    await waitFor(() => screen.getByTestId("select-accounting-group-options"));
    const ListBox = screen.getByTestId("select-accounting-group-options");
    expect(ListBox).toBeDefined();
    const label = new RegExp(`${accountingGroup.name}`, "i");
    const menuItem1 = getByText(ListBox, label);
    fireEvent.click(menuItem1);

    return this;
  }

  async createProduct(productPayload: ProductInCreationI) {
    const submitButton = screen.getByRole("button", {
      name: /Créer le produit/i,
    });
    await React.act(async () => {
      if (productPayload.accountingGroup) {
        await this.selectAccountingGroupValue(productPayload.accountingGroup);
      }
      fireEvent.change(screen.getByLabelText("Nom du produit"), {
        target: { value: productPayload.name },
      });
      if (productPayload.shortName) {
        fireEvent.change(screen.getByLabelText("Nom court"), {
          target: { value: productPayload.name },
        });
      }
      if (productPayload.freePrice) {
        fireEvent.change(screen.getByLabelText("Prix libre"), {
          target: { value: productPayload.freePrice },
        });
      }
      if (productPayload.sellPrice) {
        fireEvent.change(screen.getByLabelText("Prix de vente TTC"), {
          target: { value: getAmountValue(productPayload.sellPrice) },
        });
      }
      if (productPayload.purchasePrice) {
        fireEvent.change(screen.getByLabelText("Prix d'achat HT"), {
          target: { value: getAmountValue(productPayload.purchasePrice) },
        });
      }
      fireEvent.click(submitButton);
    });
    await this.currentPageHasTitle("Produits");
    return this;
  }
}

export const asARegisteredUser = () => new RegisteredUser();
