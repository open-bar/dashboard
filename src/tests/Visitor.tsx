import {
  fireEvent,
  render,
  renderHook,
  screen,
  waitFor,
} from "@testing-library/react";

import { expect } from "vitest";
import { TestApp } from "./test-utils";
import { Provider, createStore, useAtom } from "jotai";
import { getRedirectToAfterLogin } from "@/domains/identity/authentication/authAtom";
import React from "react";
import { IdentityContext } from "@/domains/identity/IdentityProvider/IdentityContext";
import IdentityProviderInMemory from "@/domains/identity/IdentityProvider/IdentityProvider.inMemory";
import { IdentityProvider } from "@/domains/identity/IdentityProvider/IdentityProviderI";

export class Visitor {
  protected store: ReturnType<typeof createStore>;
  protected wrapper: React.FC<React.PropsWithChildren>;
  protected identityWrapper: React.FC<React.PropsWithChildren>;
  protected identityProvider: IdentityProvider;
  constructor() {
    this.store = createStore();
    this.identityProvider = new IdentityProviderInMemory();
    this.wrapper = ({ children }: React.PropsWithChildren) => (
      <Provider store={this.store}>{children}</Provider>
    );
    this.identityWrapper = ({ children }: React.PropsWithChildren) => (
      <IdentityContext.Provider value={this.identityProvider}>
        {children}
      </IdentityContext.Provider>
    );
  }

  visisiting(path: string) {
    render(
      this.identityWrapper({
        children: <TestApp initialRoute={path} store={this.store} />,
      })
    );
    React.act(() => {});
    return this;
  }

  async isOnTheLoginPage() {
    const mainTitle = await waitFor(() =>
      screen.getByRole("heading", { level: 1 })
    );
    expect(mainTitle).toBeDefined();
    expect(mainTitle.textContent).toEqual("Connexion");
    return this;
  }

  async isOnTheRegistrationPage() {
    const mainTitle = await waitFor(() =>
      screen.getByRole("heading", { level: 1 })
    );
    expect(mainTitle).toBeDefined();
    expect(mainTitle.textContent).toEqual("Inscription");
    return this;
  }

  async isOnPageNotFound() {
    const mainTitle = await waitFor(() =>
      screen.getByRole("heading", { level: 1 })
    );
    expect(mainTitle).toBeDefined();
    expect(mainTitle.textContent).toEqual("404");
    const secondaryTitle = screen.getByRole("heading", { level: 2 });
    expect(secondaryTitle.textContent).toEqual("Page non trouvée");
    return this;
  }

  async goToRegistrationPage() {
    const linkToRegistration = screen.getByText(
      "Pas encore de compte ? Je m'inscris !"
    );
    React.act(() => {
      fireEvent.click(linkToRegistration);
    });

    await waitFor(() => this.isOnTheRegistrationPage());
    return this;
  }

  submitsLoginForm(username: string, password: string) {
    const usernameField = screen.getByLabelText(`Identifiant`);
    const passwordField = screen.getByLabelText(`Mot de passe`);
    const connexionButton = screen.getByRole("button", {
      name: /connexion/i,
    });
    expect(usernameField).toBeDefined();
    expect(passwordField).toBeDefined();

    React.act(() => {
      fireEvent.change(usernameField, { target: { value: username } });
      fireEvent.change(passwordField, {
        target: { value: password },
      });
      fireEvent.click(connexionButton);
    });

    return this;
  }

  submitsRegistrationForm(username: string, email: string, password: string) {
    const emailField = screen.getByLabelText(`Adresse e-mail`);
    const usernameField = screen.getByLabelText(`Identifiant`);
    const passwordField = screen.getByLabelText(`Mot de passe`);
    const connexionButton = screen.getByRole("button", {
      name: /inscris/i,
    });
    expect(emailField).toBeDefined();
    expect(usernameField).toBeDefined();
    expect(passwordField).toBeDefined();

    React.act(() => {
      fireEvent.change(emailField, { target: { value: email } });
      fireEvent.change(usernameField, { target: { value: username } });
      fireEvent.change(passwordField, {
        target: { value: password },
      });
      fireEvent.click(connexionButton);
    });

    return this;
  }

  async isOnTheScreensPage() {
    await waitFor(() => screen.getByText("Écrans"));
    const mainTitle = screen.getByRole("heading", { level: 1 });
    expect(mainTitle).toBeDefined();
    expect(mainTitle.textContent).toEqual("Écrans");
    return this;
  }

  disconnectFromTheApp() {
    const logoutButton = screen.getByLabelText("déconnexion");
    expect(logoutButton).toBeDefined();

    fireEvent.click(logoutButton);
    return this;
  }

  check() {
    const { result: getRedirectToResult } = renderHook(
      () => useAtom(getRedirectToAfterLogin),
      {
        wrapper: this.wrapper,
      }
    );
    expect(getRedirectToResult.current[0]).toEqual("/categories");
  }
}

export const asAVisitor = () => new Visitor();
