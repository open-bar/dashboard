import React, { PropsWithChildren } from "react";
import { useHydrateAtoms } from "jotai/utils";
import App from "@/App";
import { Provider, createStore } from "jotai";

interface HydrateAtomsProps extends PropsWithChildren {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  initialValues: any[];
}
const HydrateAtoms = ({ initialValues, children }: HydrateAtomsProps) => {
  useHydrateAtoms(initialValues);
  return <>{children}</>;
};

interface TestAppProps {
  initialRoute?: string;
  store: ReturnType<typeof createStore>;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  initialValues?: any[];
}

export const TestApp: React.FC<TestAppProps> = ({
  initialRoute,
  store,
  initialValues = [],
}) => {
  return (
    <Provider store={store}>
      <HydrateAtoms initialValues={initialValues}>
        <App
          initialEntries={
            initialRoute ? [{ pathname: initialRoute }] : undefined
          }
        />
      </HydrateAtoms>
    </Provider>
  );
};
