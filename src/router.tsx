/* eslint-disable react-refresh/only-export-components */

import { Suspense, lazy } from "react";
import { Navigate, RouteObject } from "react-router-dom";

import {
  CATALOG,
  SCREENS,
  DASHBOARD,
  LOGIN,
  PRODUCTS,
  REGISTER,
  ROUTES,
  ACCOUNTING_GROUPS,
  CREATE_PRODUCT,
  EDIT_PRODUCT,
  USERS,
  EDIT_SCREEN,
} from "./routes";
import { AuthGuard } from "./domains/identity/components/AuthGuard";
import { Loading } from "./Loading";
import EditProduct from "./domains/catalog/pages/EditProduct";

const PageNotFound = lazy(() => import("./PageNotFound"));

const Login = lazy(() => import("./domains/identity/pages/Login"));
const Register = lazy(() => import("./domains/identity/pages/Register"));
const Users = lazy(() => import("./domains/identity/pages/Users"));

const WithCatalogProvider = lazy(
  () => import("./domains/catalog/providers/WithCatalogProvider")
);
const EditScreen = lazy(() => import("./domains/catalog/pages/EditScreen"));
const Screens = lazy(() => import("./domains/catalog/pages/Screens"));
const CreateProduct = lazy(
  () => import("./domains/catalog/pages/CreateProduct")
);
const Products = lazy(() => import("./domains/catalog/pages/Products"));
const AccountingGroups = lazy(
  () => import("./domains/catalog/pages/AccountingGroups")
);

export const allRoutes: RouteObject[] = [
  {
    path: "/",
    errorElement: <PageNotFound />,
    children: [
      {
        path: ROUTES[LOGIN].substring(1),
        element: (
          <Suspense fallback={<Loading />}>
            <Login />
          </Suspense>
        ),
      },
      {
        path: ROUTES[REGISTER].substring(1),
        element: (
          <Suspense fallback={<Loading />}>
            <Register />
          </Suspense>
        ),
      },
      {
        path: ROUTES[DASHBOARD].substring(1),
        element: <AuthGuard />,
        children: [
          {
            path: ROUTES[CATALOG].substring(1),
            element: (
              <Suspense fallback={<Loading />}>
                <WithCatalogProvider />
              </Suspense>
            ),
            children: [
              {
                path: ROUTES[EDIT_SCREEN].split("/catalog/").pop(),
                element: (
                  <Suspense fallback={<Loading />}>
                    <EditScreen />
                  </Suspense>
                ),
              },
              {
                path: ROUTES[SCREENS].split("/").pop(),
                element: (
                  <Suspense fallback={<Loading />}>
                    <Screens />
                  </Suspense>
                ),
              },
              {
                path: ROUTES[CREATE_PRODUCT].split("/").pop(),
                element: (
                  <Suspense fallback={<Loading />}>
                    <CreateProduct />
                  </Suspense>
                ),
              },
              {
                path: ROUTES[EDIT_PRODUCT].split("/catalog/").pop(),
                element: (
                  <Suspense fallback={<Loading />}>
                    <EditProduct />
                  </Suspense>
                ),
              },
              {
                path: ROUTES[PRODUCTS].split("/").pop(),
                element: (
                  <Suspense fallback={<Loading />}>
                    <Products />
                  </Suspense>
                ),
              },
              {
                path: ROUTES[ACCOUNTING_GROUPS].split("/").pop(),
                element: (
                  <Suspense fallback={<Loading />}>
                    <AccountingGroups />
                  </Suspense>
                ),
              },
            ],
          },
          {
            path: ROUTES[USERS].split("/").pop(),
            element: (
              <Suspense fallback={<Loading />}>
                <Users />
              </Suspense>
            ),
          },

          {
            path: "",
            element: <Navigate to={ROUTES[PRODUCTS]} replace />,
          },
        ],
      },
    ],
  },
];
