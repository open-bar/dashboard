import { Box, Typography, styled } from "@mui/material";
import React, { PropsWithChildren, ReactNode } from "react";

interface Props extends PropsWithChildren {
  title: ReactNode;
  beforeTitle?: ReactNode;
}

const TitleContainer = styled(Box)({
  display: "flex",
  alignItems: "flex-start",
  justifyContent: "space-between",
  marginBottom: "32px",
  flexWrap: "wrap",
  gap: "16px",
});

const Title = styled(Typography)({
  margin: 0,
  flex: "1 1 auto",
});

const ChildrenContainer = styled(Box)({
  flex: "1 1 auto",
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-end",
});

export const MainTitle: React.FC<Props> = ({
  beforeTitle,
  title,
  children,
}) => (
  <TitleContainer>
    {beforeTitle}
    <Title variant="h2" role="heading" aria-level={1}>
      {title}
    </Title>
    {children && <ChildrenContainer>{children}</ChildrenContainer>}
  </TitleContainer>
);
