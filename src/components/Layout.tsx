import React, { useCallback } from "react";
import { Header } from "./Header";
import { MainMenu } from "./MainMenu";
import { Outlet } from "react-router-dom";
import { useAtom } from "jotai";
import { atomWithToggle } from "@/utils/atomWithToggle";

const openAtom = atomWithToggle(false);

export const Layout: React.FC = () => {
  const [open, toggleMenu] = useAtom(openAtom);
  const closeMenu = useCallback(() => {
    toggleMenu(false);
  }, [toggleMenu]);
  return (
    <>
      <Header toggleMenu={toggleMenu} />
      <MainMenu open={open} onClose={closeMenu} />
      <Outlet />
    </>
  );
};
