import {
  Box,
  Drawer,
  List,
  ListSubheader,
  Typography,
  styled,
} from "@mui/material";

import ShoppingCartOutlinedIcon from "@mui/icons-material/ShoppingCartOutlined";
import PeopleOutlined from "@mui/icons-material/PeopleOutlined";
import CategoryOutlined from "@mui/icons-material/CategoryOutlined";
import PercentOutlined from "@mui/icons-material/PercentOutlined";

import React from "react";
import { MenuItem } from "./MenuItem";
import { NavLink, generatePath } from "react-router-dom";
import { SCREENS, PRODUCTS, ROUTES, ACCOUNTING_GROUPS } from "@/routes";

interface Props {
  open: boolean;
  onClose: () => void;
}

const MenuContainer = styled(Box)({
  width: "300px",
  height: "100%",
  maxWidth: "100%",
});

const TitleContainer = styled(Typography)({
  minHeight: 64,
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
});

export const MainMenu: React.FC<Props> = ({ onClose, open }) => {
  return (
    <Drawer open={open} onClose={onClose}>
      <MenuContainer role="presentation">
        <TitleContainer variant="h6">Openbar</TitleContainer>
        <List
          component="nav"
          aria-labelledby="catalog-subheader"
          subheader={
            <ListSubheader component="div" id="catalog-subheader">
              Catalogue
            </ListSubheader>
          }
        >
          <NavLink to={generatePath(ROUTES[PRODUCTS])} onClick={onClose}>
            {({ isActive }) => (
              <MenuItem isActive={isActive} icon={<ShoppingCartOutlinedIcon />}>
                Produits
              </MenuItem>
            )}
          </NavLink>
          <NavLink
            to={generatePath(ROUTES[ACCOUNTING_GROUPS])}
            onClick={onClose}
          >
            {({ isActive }) => (
              <MenuItem isActive={isActive} icon={<PercentOutlined />}>
                Groupes comptables
              </MenuItem>
            )}
          </NavLink>
          <NavLink to={generatePath(ROUTES[SCREENS])} onClick={onClose}>
            {({ isActive }) => (
              <MenuItem isActive={isActive} icon={<CategoryOutlined />}>
                Écrans
              </MenuItem>
            )}
          </NavLink>
        </List>
        <List
          component="nav"
          aria-labelledby="settings-subheader"
          subheader={
            <ListSubheader component="div" id="settings-subheader">
              Configuration
            </ListSubheader>
          }
        >
          <NavLink to="/users" onClick={onClose}>
            {({ isActive }) => (
              <MenuItem isActive={isActive} icon={<PeopleOutlined />}>
                Utilisateurs
              </MenuItem>
            )}
          </NavLink>
        </List>
      </MenuContainer>
    </Drawer>
  );
};
