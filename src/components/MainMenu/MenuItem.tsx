import { ListItemButton, ListItemIcon, ListItemText } from "@mui/material";
import { PropsWithChildren, ReactNode } from "react";

interface Props extends PropsWithChildren {
  isActive: boolean;
  icon?: ReactNode;
}

export const MenuItem: React.FC<Props> = ({ isActive, icon, children }) => {
  return (
    <ListItemButton selected={isActive}>
        {icon && <ListItemIcon>{icon}</ListItemIcon>}
      <ListItemText>{children}</ListItemText>
    </ListItemButton>
  );
};
