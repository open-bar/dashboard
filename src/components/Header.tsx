import {
  AppBar,
  Container,
  IconButton,
  Toolbar,
  Typography,
  styled,
} from "@mui/material";
import { ExitToAppOutlined, Menu as MenuIcon } from "@mui/icons-material";
import React from "react";
import { useLogout } from "@/domains/identity/hooks";

interface Props {
  toggleMenu?: () => void;
}

const StyledToolbar = styled(Toolbar)(({ theme }) => ({
  paddingLeft: 0,
  paddingRight: 0,
  [theme.breakpoints.up("sm")]: {
    paddingLeft: 0,
    paddingRight: 0,
  },
}));

const StyledAppBar = styled(AppBar)({
  marginBottom: "48px",
});

export const Header: React.FC<Props> = ({ toggleMenu }) => {
  const { isAuthenticated, logout } = useLogout();
  return (
    <StyledAppBar position="sticky">
      <Container>
        <StyledToolbar>
          {toggleMenu && (
            <IconButton
              size="large"
              edge="start"
              color="inherit"
              aria-label="menu"
              onClick={toggleMenu}
              sx={{ mr: 2 }}
            >
              <MenuIcon />
            </IconButton>
          )}
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            Openbar
          </Typography>
          {isAuthenticated && (
            <IconButton
              color="inherit"
              onClick={logout}
              role="button"
              aria-label="déconnexion"
            >
              <ExitToAppOutlined />
            </IconButton>
          )}
        </StyledToolbar>
      </Container>
    </StyledAppBar>
  );
};
