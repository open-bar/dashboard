import { keyframes, styled } from "@mui/material";
import React from "react";

const LoaderContainer = styled("div")(({ theme }) => ({
  boxSizing: "border-box",
  display: "inline-block",
  position: "relative",
  width: "80px",
  height: "80px",
  color: theme.palette.primary.main,
}));

const rotate = keyframes`
  0% {
    transform: rotate(-180deg);
  }

  100% {
    transform: rotate(180deg);
  }
`;

const Ball = styled("i")({
  boxSizing: "border-box",
  animation: `${rotate} 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite`,
  transformOrigin: `40px 40px`,
  position: "absolute",
  "&:after": {
    content: '" "',
    boxSizing: "border-box",
    display: "block",
    position: "absolute",
    width: "8px",
    height: "8px",
    borderRadius: "50%",
    background: "currentColor",
    margin: "-3.6px 0 0 -3.6px",
  },
});
const Ball1 = styled(Ball)({
  animationDelay: "-0.036s",
  "&:after": {
    top: "62.62742px",
    left: "62.62742px",
  },
});
const Ball2 = styled(Ball)({
  animationDelay: "-0.072s",
  "&:after": {
    top: "67.71281px",
    left: "56px",
  },
});
const Ball3 = styled(Ball)({
  animationDelay: "-0.108s",
  "&:after": {
    top: "70.90963px",
    left: "48.28221px",
  },
});
const Ball4 = styled(Ball)({
  animationDelay: "-0.144s",
  "&:after": {
    top: "72px",
    left: "40px",
  },
});
const Ball5 = styled(Ball)({
  animationDelay: "-0.18s",
  "&:after": {
    top: "70.90963px",
    left: "31.71779px",
  },
});
const Ball6 = styled(Ball)({
  animationDelay: "-0.216s",
  "&:after": {
    top: "67.71281px",
    left: "24px",
  },
});
const Ball7 = styled(Ball)({
  animationDelay: "-0.252s",
  "&:after": {
    top: "62.62742px",
    left: "17.37258px",
  },
});
const Ball8 = styled(Ball)({
  animationDelay: "-0.288s",
  "&:after": {
    top: "56px",
    left: "12.28719px",
  },
});

export const Loader: React.FC = () => (
  <LoaderContainer>
    <Ball1 />
    <Ball2 />
    <Ball3 />
    <Ball4 />
    <Ball5 />
    <Ball6 />
    <Ball7 />
    <Ball8 />
  </LoaderContainer>
);
