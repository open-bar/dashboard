import { Link, SxProps, Typography } from "@mui/material";
import React from "react";

interface Props {
  sx: SxProps;
}

export const Copyleft: React.FC<Props> = (props) => {
  return (
    <Typography
      variant="body2"
      color="text.secondary"
      align="center"
      {...props}
    >
      {"Copyleft 🄯 "}
      <Link color="inherit" href="#">
        Open bar
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
};
