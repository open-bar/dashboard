import { Alert, Box, Paper, styled } from "@mui/material";
import { ReactNode, useCallback } from "react";
import { DataTableItem, ItemProps, ItemWithId } from "./DataTableItem";
import { OrderDirection } from "./OrderDirection";
import { DataTableHeader } from "./DataTableHeader";
import { WithSortableData } from "./WithSortableData";

export type DataTableProps<T extends ItemWithId> = {
  data: T[] | undefined;
  classNames?: string;
  defaultOrderBy?: string;
  defaultOrderDirection?: OrderDirection;
  onRequestSort?: (orderBy: string, orderDirection: OrderDirection) => void;
  hidePagination?: boolean;
  withAggregatedFooter?: boolean;
  noItemsMessage?: ReactNode;
} & ItemProps<T>;

const StyledPaper = styled(Paper)({
  overflowY: "hidden",
});

const StyledBox = styled(Box)({
  display: "flex",
  flexDirection: "column",
  alignItems: "stretch",
});

export const DataTable = <T extends ItemWithId>({
  data,
  noItemsMessage,
  ...props
}: DataTableProps<T>) => {
  const renderValues = useCallback(
    (dataLine: T, index: number) => {
      return (
        <DataTableItem
          key={`${dataLine.id}`}
          index={index}
          item={dataLine}
          {...props}
        />
      );
    },
    [props]
  );
  return (
    <StyledPaper>
      <StyledBox>
        <DataTableHeader columns={props.columns} />
        {props.sortable && data ? (
          <WithSortableData data={data} moveItemAfter={props.moveItemAfter}>
            {data?.map(renderValues)}
          </WithSortableData>
        ) : (
          data &&
          (data.length > 0 ? (
            data.map(renderValues)
          ) : (
            <Alert severity="info">{noItemsMessage ?? "Aucun élément"}</Alert>
          ))
        )}
      </StyledBox>
    </StyledPaper>
  );
};
