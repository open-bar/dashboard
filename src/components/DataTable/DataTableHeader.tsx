import { useCallback } from "react";
import { DataTableColumn } from "./DataTableColumn";
import { Box, styled } from "@mui/material";

type Props<T> = {
  columns: DataTableColumn<T>[];
};

const HeaderContainer = styled(Box)(({ theme }) => ({
  display: "flex",
  alignItems: "stretch",
  justifyContent: "space-between",
  padding: "0 16px",
  height: "56px",
  minWidth: "100%",
  boxSizing: "border-box",
  width: "fit-content",
  borderBottom: `1px solid ${theme.palette.divider}`,
}));

const HeaderColumn = styled(Box)(({ theme }) => ({
  ...theme.typography.body1,
  display: "flex",
  alignItems: "center",
  fontSize: "14px",
  fontWeight: 500,
  padding: "8px 0",
}));

export const DataTableHeader = <T,>({ columns }: Props<T>) => {
  const renderHeader = useCallback(
    (column: DataTableColumn<T>) => (
      <HeaderColumn
        key={column.id}
        sx={{
          flex: `${column.flexGrow || 0} ${column.flexShrink || 0} ${
            column.width
          }px`,
          minWidth: `${column.width}px`,
        }}
        style={column.style}
      >
        {column.label}
      </HeaderColumn>
    ),
    []
  );
  return <HeaderContainer>{columns.map(renderHeader)}</HeaderContainer>;
};
