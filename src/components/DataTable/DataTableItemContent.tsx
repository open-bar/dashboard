import { DragHandleRounded } from "@mui/icons-material";
import { DataTableColumn } from "./DataTableColumn";
import { Box, ListItemButton, Typography, styled } from "@mui/material";
import { SyntheticListenerMap } from "@dnd-kit/core/dist/hooks/utilities";
import { ItemWithId } from "./DataTableItem";

type SortableItem = {
  sortable: true;
  setNodeRef: (node: HTMLElement | null) => void;
  isDragging: boolean;
  listeners: SyntheticListenerMap | undefined;
};

type NotSortableItem = {
  sortable?: false;
  setNodeRef?: never;
  isDragging?: never;
  listeners?: never;
};

type Props<T extends ItemWithId> = {
  handleClickRow?: (item: T) => void;
  item: T;
  columns: DataTableColumn<T>[];
  index: number;
  style?: React.CSSProperties;
} & (SortableItem | NotSortableItem);

type ListItemProps = {
  isDragging: boolean;
  sortable?: boolean;
};

const StyledListItem = styled(ListItemButton, {
  shouldForwardProp: (prop) => prop !== "isDragging" && prop !== "sortable",
})<ListItemProps>(({ theme, isDragging, sortable }) => ({
  ...theme.typography.body1,
  display: "flex",
  alignItems: "center",
  padding: "8px 16px",
  width: "auto",
  borderBottom: `1px solid ${theme.palette.divider}`,
  alignSelf: "stretch",
  minWidth: "fit-content",
  touchAction: sortable ? "manipulation" : "auto",
  "&:last-child": {
    borderBottom: "none",
  },
  ...(isDragging && {
    opacity: 0.5,
  }),
}));

export const DataTableItemContent = <T extends ItemWithId>({
  handleClickRow,
  columns,
  item,
  index,
  sortable,
  setNodeRef,
  listeners,
  isDragging,
  style,
}: Props<T>) => {
  const onClick = handleClickRow ? () => handleClickRow(item) : undefined;
  return (
    <StyledListItem
      isDragging={sortable ? isDragging : false}
      ref={sortable ? setNodeRef : undefined}
      style={style}
      sortable={sortable}
      {...listeners}
      onClick={onClick}
    >
      {sortable && (
        <Box sx={{ flex: `0 0 40px` }}>
          <DragHandleRounded onClick={(e) => e.stopPropagation()} />
        </Box>
      )}
      {columns.map((column) => (
        <Box
          key={`${item.id}-${column.id}`}
          sx={{
            flex: `${column.flexGrow || 0} ${column.flexShrink || 0} ${
              column.width
            }px`,
            minWidth: `${column.width}px`,
          }}
        >
          {column.renderCell ? (
            column.renderCell(item, index)
          ) : (
            <Typography component="div">
              {column.getValue ? column.getValue(item) : `${item[column.key]}`}
            </Typography>
          )}
        </Box>
      ))}
    </StyledListItem>
  );
};
