import { DataTableColumn } from "./DataTableColumn";
import { DataTableItemContent } from "./DataTableItemContent";
import { useSortable } from "@dnd-kit/sortable";
import { CSS } from "@dnd-kit/utilities";
import { ItemWithId } from "./DataTableItem";

type Props<T extends ItemWithId> = {
  handleClickRow?: (item: T) => void;
  item: T;
  columns: DataTableColumn<T>[];
  index: number;
  moveItemAfter: (item: T, afterItem: T) => void;
};

export const DataTableItemSortable = <T extends ItemWithId>({
  ...props
}: Props<T>) => {
  const {
    attributes,
    listeners,
    setNodeRef,
    isDragging,
    transform,
    transition,
  } = useSortable({ id: props.item.id });

  const style = {
    transform: CSS.Transform.toString(transform),
    transition,
  };

  return (
    <DataTableItemContent
      {...props}
      {...attributes}
      listeners={listeners}
      isDragging={isDragging}
      setNodeRef={setNodeRef}
      style={style}
      sortable
    />
  );
};
