type GetValueFunc<T> = (o: T) => string | number | null;
type RenderCellFunc<T> = (o: T, index: number) => React.ReactNode;

type DataTableColumnWithKey<T> = {
  key: keyof T;
  getValue?: never;
};
type DataTableColumnWithGetValue<T> = {
  key?: never;
  getValue: GetValueFunc<T>;
};

type DataTableColumnValue<T> =
  | DataTableColumnWithKey<T>
  | DataTableColumnWithGetValue<T>;

export type DataTableColumn<T> = DataTableColumnValue<T> & {
  label: React.ReactNode;
  id: string;
  width: number;
  flexGrow?: number;
  flexShrink?: number;
  renderCell?: RenderCellFunc<T>;
  sorter?: boolean;
  style?: React.CSSProperties;
  visible?: boolean;
  aggregationFunction?: (items: T[]) => React.ReactNode;
};
