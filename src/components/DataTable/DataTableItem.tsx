import { DataTableColumn } from "./DataTableColumn";
import { DataTableItemSortable } from "./DataTableItemSortable";
import { DataTableItemContent } from "./DataTableItemContent";

type SortableItemsProps<T> = {
  sortable: true;
  moveItemAfter: (item: T, afterItem: T) => void;
};

type NotSortableItemsProps = {
  sortable?: false;
  moveItemAfter?: never;
};

export type ItemWithId = {
  id: string;
};

export type ItemProps<T extends ItemWithId> = {
  handleClickRow?: (item: T) => void;
  columns: DataTableColumn<T>[];
} & (SortableItemsProps<T> | NotSortableItemsProps);

type Props<T extends ItemWithId> = ItemProps<T> & {
  item: T;
  index: number;
};

export const DataTableItem = <T extends ItemWithId>({
  sortable,
  moveItemAfter,
  ...props
}: Props<T>) => {
  if (sortable) {
    return <DataTableItemSortable moveItemAfter={moveItemAfter} {...props} />;
  }
  return <DataTableItemContent {...props} />;
};
