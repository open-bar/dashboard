import {
  DndContext,
  DragEndEvent,
  DragStartEvent,
  MouseSensor,
  TouchSensor,
  UniqueIdentifier,
  closestCenter,
  useSensor,
  useSensors,
} from "@dnd-kit/core";
import { PropsWithChildren, useCallback, useState } from "react";
import {
  SortableContext,
  verticalListSortingStrategy,
} from "@dnd-kit/sortable";
import { ItemWithId } from "./DataTableItem";

type Props<T extends ItemWithId> = PropsWithChildren & {
  data: T[];
  moveItemAfter: (item: T, afterItem: T) => void;
};

export const WithSortableData = <T extends ItemWithId>({
  data,
  moveItemAfter,
  children,
}: Props<T>) => {
  const sensors = useSensors(
    useSensor(MouseSensor, { activationConstraint: { distance: 8 } }),
    useSensor(TouchSensor, {
      activationConstraint: {
        distance: 8,
      },
    })
  );
  const [, setActiveId] = useState<UniqueIdentifier | null>(null);

  const handleDragStart = useCallback((event: DragStartEvent) => {
    setActiveId(event.active.id);
  }, []);

  const handleDragEnd = useCallback(
    (event: DragEndEvent) => {
      if (!data) return;
      const { active, over } = event;
      if (over && active.id !== over.id) {
        const activeItem = data.find((item) => item.id === active.id);
        const overItem = data.find((item) => item.id === over.id);
        if (activeItem && overItem) {
          moveItemAfter(activeItem, overItem);
        }
      }

      setActiveId(null);
    },
    [data, moveItemAfter]
  );

  const handleDragCancel = useCallback(() => {
    setActiveId(null);
  }, []);

  return (
    <DndContext
      sensors={sensors}
      collisionDetection={closestCenter}
      onDragStart={handleDragStart}
      onDragEnd={handleDragEnd}
      onDragCancel={handleDragCancel}
    >
      <SortableContext items={data} strategy={verticalListSortingStrategy}>
        {children}
      </SortableContext>
    </DndContext>
  );
};
