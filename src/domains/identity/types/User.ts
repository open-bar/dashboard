import * as t from "io-ts";
import { Opaque } from "@/utils/Opaque";
import { UserRole } from "./UserRole";

export type UserId = Opaque<string, { readonly T: unique symbol }>;

export const UserIO = t.type({
  id: t.string,
  username: t.string,
  role: t.string,
});

export interface UserI {
  id: UserId;
  username: string;
  role: UserRole;
}
