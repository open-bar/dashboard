import { UserRole } from "./UserRole";

export interface InviteUserPayload {
  email: string;
  role: UserRole;
}
