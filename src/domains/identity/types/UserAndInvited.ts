import { InvitedUserI, InvitedUserId } from "./InvitedUser";
import { UserI, UserId } from "./User";

export enum UserKind {
  INVITED_USER,
  ACTUAL_USER,
}

export type ActualUserKind = {
  id: UserId;
  type: UserKind.ACTUAL_USER;
  value: UserI;
};
export type InvitedUserKind = {
  id: InvitedUserId;
  type: UserKind.INVITED_USER;
  value: InvitedUserI;
};

export type UserAndInvitedUserI = ActualUserKind | InvitedUserKind;
