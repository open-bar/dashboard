import { TypeOf } from "io-ts";
import { InvitedUserI, InvitedUserIO, InvitedUserId } from "./InvitedUser";
import { parseUserRole } from "./parseUserRole";
import {
  ParsingErrorType,
  Result,
  ResultSuccess,
  genericParser,
  isResultSuccess,
} from "@/utils";

const dataToInvitedUser = (
  data: TypeOf<typeof InvitedUserIO>
): InvitedUserI => ({
  ...data,
  id: data.id as InvitedUserId,
  role: parseUserRole(data.role),
});

export const parseInvitedUser = (
  data: unknown
): Result<InvitedUserI, ParsingErrorType> => {
  const userResult = genericParser(data, InvitedUserIO);
  if (isResultSuccess(userResult)) {
    return ResultSuccess(dataToInvitedUser(userResult.value));
  }
  return userResult;
};
