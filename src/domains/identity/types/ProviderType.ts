export enum ProviderType {
  IN_MEMORY = "IN_MEMORY",
  ACTUAL = "ACTUAL",
}
