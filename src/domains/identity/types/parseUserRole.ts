import { UserRole } from "./UserRole";

export const parseUserRole = (strRole: string): UserRole => {
  if (Object.values(UserRole).includes(strRole as UserRole)) {
    return strRole as UserRole;
  }
  return UserRole.NONE;
};
