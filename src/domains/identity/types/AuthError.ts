import * as t from "io-ts";
import {
  CustomFetchError,
  FetchError,
  FetchErrorType,
  ParsingErrorType,
} from "@/utils/errors";
import { genericParser } from "@/utils/genericParser";
import { Result, ResultSuccess, isResultSuccess } from "@/utils/Result";

export enum AuthErrorType {
  INVALID_LOGIN_OR_PASSWORD = "INVALID_LOGIN_OR_PASSWORD",
  UNKNOWN_ERROR = "UNKNOWN_ERROR",
}

const AuthErrorTypeIO = t.keyof({
  // using "keyof" for better performance instead of "union"
  [AuthErrorType.INVALID_LOGIN_OR_PASSWORD]: null,
  [AuthErrorType.UNKNOWN_ERROR]: null,
});

export const AuthErrorIO = t.type({
  error: AuthErrorTypeIO,
});

interface AuthErrorI extends CustomFetchError {
  error: AuthErrorType;
}

export type AuthError = FetchError | AuthErrorI;

export const parseAuthError = (
  data: unknown
): Result<AuthErrorI, ParsingErrorType> => {
  const authErrorResult = genericParser(data, AuthErrorIO);
  if (isResultSuccess(authErrorResult)) {
    return ResultSuccess({
      ...authErrorResult.value,
      type: FetchErrorType.CUSTOM_ERROR,
    });
  }
  return authErrorResult;
};
