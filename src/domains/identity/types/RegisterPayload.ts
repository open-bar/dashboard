import { LoginPayload } from "./LoginPayload";

export interface RegisterPayload extends LoginPayload {
  email: string;
}
