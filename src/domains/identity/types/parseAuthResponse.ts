import { UserI, UserIO, UserId } from "./User";
import { genericParser } from "@/utils/genericParser";
import { AuthResponseI, AuthResponseIO } from "./AuthResponse";
import { Result, ResultSuccess, isResultSuccess } from "@/utils/Result";
import { ParsingErrorType } from "@/utils/errors";
import { TypeOf } from "io-ts";
import { parseUserRole } from "./parseUserRole";

const dataToUser = (data: TypeOf<typeof UserIO>): UserI => ({
  ...data,
  id: data.id as UserId,
  role: parseUserRole(data.role),
});

export const parseUser = (data: unknown): Result<UserI, ParsingErrorType> => {
  const userResult = genericParser(data, UserIO);
  if (isResultSuccess(userResult)) {
    return ResultSuccess(dataToUser(userResult.value));
  }
  return userResult;
};

export const parseAuthResponse = (
  data: unknown
): Result<AuthResponseI, ParsingErrorType> => {
  const authResponseResult = genericParser(data, AuthResponseIO);
  if (isResultSuccess(authResponseResult)) {
    return ResultSuccess({
      ...authResponseResult.value,
      user: dataToUser(authResponseResult.value.user),
    });
  }
  return authResponseResult;
};
