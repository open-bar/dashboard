import * as t from "io-ts";
import { Opaque } from "@/utils/Opaque";
import { UserRole } from "./UserRole";

export type InvitedUserId = Opaque<string, { readonly T: unique symbol }>;

export const InvitedUserIO = t.type({
  id: t.string,
  email: t.string,
  role: t.string,
});

export interface InvitedUserI {
  id: InvitedUserId;
  email: string;
  role: UserRole;
}
