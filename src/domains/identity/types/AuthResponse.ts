import * as t from "io-ts";
import { UserI, UserIO } from "./User";

export const AuthResponseIO = t.type({
  token: t.string,
  user: UserIO,
});

export type AuthResponseI = {
  token: string;
  user: UserI;
};
