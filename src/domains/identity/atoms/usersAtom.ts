import { atomWithImmer } from "jotai-immer";
import { atom } from "jotai";
import { UserI, UserId } from "../types/User";

export const usersAtom = atomWithImmer<UserI[] | null>(null);
export const addUserAtom = atom(usersAtom, (get, set, user: UserI) => {
  const users = get(usersAtom);
  set(usersAtom, [
    ...(users ? users.filter((u) => u.id !== user.id) : []),
    user,
  ]);
});
export const removeUserAtom = atom(usersAtom, (get, set, userId: UserId) => {
  const users = get(usersAtom);
  set(usersAtom, [...(users ? users.filter((u) => u.id !== userId) : [])]);
  return users;
});
export const updateUserAtom = atom(
  null,
  (get, set, userId: UserId, payload: Partial<Omit<UserI, "id">>) => {
    const users = get(usersAtom);
    if (users) {
      set(
        usersAtom,
        users.map((user) =>
          user.id === userId ? { ...user, ...payload } : user
        ) ?? null
      );
      return users.find((user) => user.id === userId) ?? null;
    }
    return null;
  }
);
