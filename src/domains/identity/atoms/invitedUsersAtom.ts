import { atomWithImmer } from "jotai-immer";
import { atom } from "jotai";
import { InvitedUserI, InvitedUserId } from "../types";

export const invitedUsersAtom = atomWithImmer<InvitedUserI[] | null>(null);
export const addInvitedUserAtom = atom(
  invitedUsersAtom,
  (get, set, invitedUser: InvitedUserI) => {
    const invitedUsers = get(invitedUsersAtom);
    set(invitedUsersAtom, [
      ...(invitedUsers
        ? invitedUsers.filter((u) => u.id !== invitedUser.id)
        : []),
      invitedUser,
    ]);
  }
);
export const removeInvitedUserAtom = atom(
  invitedUsersAtom,
  (get, set, invitedUserId: InvitedUserId) => {
    const invitedUsers = get(invitedUsersAtom);
    set(invitedUsersAtom, [
      ...(invitedUsers
        ? invitedUsers.filter((u) => u.id !== invitedUserId)
        : []),
    ]);
    return invitedUsers;
  }
);
export const updateInvitedUserAtom = atom(
  null,
  (
    get,
    set,
    invitedUserId: InvitedUserId,
    payload: Partial<Omit<InvitedUserI, "id">>
  ) => {
    const invitedUsers = get(invitedUsersAtom);
    if (invitedUsers) {
      set(
        invitedUsersAtom,
        invitedUsers.map((user) =>
          user.id === invitedUserId ? { ...user, ...payload } : user
        ) ?? null
      );
      return invitedUsers.find((user) => user.id === invitedUserId) ?? null;
    }
    return null;
  }
);
