import { atom } from "jotai";

import { atomWithImmer } from "jotai-immer";
import { UserI } from "../types/User";
import { AuthError } from "../types";

interface AuthAtom {
  currentUser: UserI | null;
  redirectToAfterLogin?: string;

  loginInProgress: boolean;
  loginError?: AuthError;

  registrationInProgress: boolean;
  registrationError?: AuthError;
}

export const authState = atomWithImmer<AuthAtom>({
  currentUser: null,
  loginInProgress: false,
  registrationInProgress: false,
});

export const logoutAtom = atom(
  (get) => !!get(authState).currentUser,
  (get, set) => {
    return set(authState, {
      ...get(authState),
      currentUser: null,
    });
  }
);

export const getRedirectToAfterLogin = atom((get) => {
  return get(authState).redirectToAfterLogin;
});

export const startLoginAtom = atom(
  (get) => get(authState).loginInProgress,
  (get, set) => {
    return set(authState, {
      ...get(authState),
      loginInProgress: true,
      loginError: undefined,
    });
  }
);

export const loginErrorAtom = atom(
  (get) => get(authState).loginError,
  (get, set, error: AuthError) => {
    return set(authState, {
      ...get(authState),
      loginInProgress: false,
      loginError: error,
    });
  }
);

export const startRegistrationAtom = atom(
  (get) => get(authState).registrationInProgress,
  (get, set) => {
    return set(authState, {
      ...get(authState),
      registrationInProgress: true,
      registrationError: undefined,
    });
  }
);

export const registrationErrorAtom = atom(
  (get) => get(authState).registrationError,
  (get, set, error: AuthError) => {
    return set(authState, {
      ...get(authState),
      registrationInProgress: false,
      registrationError: error,
    });
  }
);

export const currentUserAtom = atom(
  (get) => !!get(authState).currentUser,
  (get, set, currentUser: UserI | null) => {
    return set(authState, {
      ...get(authState),
      currentUser,
      loginInProgress: false,
      registrationInProgress: false,
    });
  }
);

export const redirectToAfterLoginAtom = atom(
  (get) => !!get(authState).currentUser,
  (get, set, redirectToAfterLogin: string | undefined) => {
    return set(authState, { ...get(authState), redirectToAfterLogin });
  }
);

export const isAuthenticated = atom((get) => !!get(authState).currentUser);
