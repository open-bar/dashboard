import { isResultSuccess } from "@/utils/Result";
import { AuthOptions } from "../hooks/useHandleUserResult";
import { AuthResponseI, parseAuthResponse } from "../types";

const userKey = "_u";

export const saveAuthResponse = (auth: AuthResponseI, options: AuthOptions) => {
  if (
    options.saveCredentials &&
    typeof localStorage === "object" &&
    typeof localStorage.setItem === "function"
  ) {
    localStorage.setItem(userKey, JSON.stringify(auth));
  } else if (
    typeof sessionStorage === "object" &&
    typeof sessionStorage.setItem === "function"
  ) {
    sessionStorage.setItem(userKey, JSON.stringify(auth));
  }
};

const handleStoredAuthResponse = (
  storedAuthResponse: string,
  storage: Storage
): AuthResponseI | null => {
  try {
    const parsedAuthResponse = JSON.parse(storedAuthResponse);
    const result = parseAuthResponse(parsedAuthResponse);
    if (isResultSuccess(result)) {
      return result.value;
    } else {
      storage.removeItem(userKey);
    }
  } catch {
    storage.removeItem(userKey);
  }
  return null;
};

export const getSavedAuthResponse = (): AuthResponseI | null => {
  if (
    typeof sessionStorage === "object" &&
    typeof sessionStorage.getItem === "function"
  ) {
    const storedAuthResponse = sessionStorage.getItem(userKey);
    if (storedAuthResponse) {
      const result = handleStoredAuthResponse(
        storedAuthResponse,
        sessionStorage
      );
      if (result) {
        return result;
      }
    }
  }
  if (
    typeof localStorage === "object" &&
    typeof localStorage.getItem === "function"
  ) {
    const storedAuthResponse = localStorage.getItem(userKey);
    if (storedAuthResponse) {
      const result = handleStoredAuthResponse(storedAuthResponse, localStorage);
      if (result) {
        return result;
      }
    }
  }
  return null;
};
