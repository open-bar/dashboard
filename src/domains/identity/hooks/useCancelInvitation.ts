import { useAtom } from "jotai";
import { useIdentityProvider } from "../IdentityProvider";
import { addInvitedUserAtom, removeInvitedUserAtom } from "../atoms";
import { useCreateSnackbarMessage } from "@/domains/common/snackbar/hooks";
import { useCallback } from "react";
import { InvitedUserI } from "../types";
import { isResultError } from "@/utils";

export const useCancelInvitation = (invitedUser: InvitedUserI) => {
  const identityProvider = useIdentityProvider();
  const [, removeInvitedUser] = useAtom(removeInvitedUserAtom);
  const [, addInvitedUser] = useAtom(addInvitedUserAtom);
  const createSnackbarMessage = useCreateSnackbarMessage();

  return useCallback(async () => {
    const previousState = removeInvitedUser(invitedUser.id);
    const result = await identityProvider.cancelInvitation(invitedUser.id);
    if (isResultError(result)) {
      createSnackbarMessage({
        severity: "error",
        message: `Une erreur innattendue est survenue lors de l'annulation de l'invitation de ${invitedUser.email}`,
      });
      if (previousState) {
        addInvitedUser(invitedUser);
      }
    } else {
      createSnackbarMessage({
        severity: "success",
        message: `L'invitation de ${invitedUser.email} a bien été annulée`,
      });
    }
  }, [
    identityProvider,
    removeInvitedUser,
    invitedUser,
    addInvitedUser,
    createSnackbarMessage,
  ]);
};
