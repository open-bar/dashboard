import { useCallback, useState } from "react";
import { useIdentityProvider } from "../IdentityProvider";
import { useAtom } from "jotai";
import { addInvitedUserAtom } from "../atoms";
import { FetchError, resultPipe } from "@/utils";
import { InviteUserPayload } from "../types/InviteUserPayload";

export const useInviteUser = () => {
  const [, addInvitedUser] = useAtom(addInvitedUserAtom);
  const [error, setError] = useState<FetchError | null>(null);
  const [loading, setLoading] = useState<boolean>(false);
  const identityProvider = useIdentityProvider();
  const inviteUser = useCallback(
    async (payload: InviteUserPayload) => {
      setLoading(true);
      const result = await identityProvider.inviteUser(payload);
      resultPipe(result, addInvitedUser, setError);
      setLoading(false);
    },
    [identityProvider, addInvitedUser]
  );

  if (error) {
    return {
      error,
      loading: false,
      inviteUser,
    };
  } else if (loading) {
    return {
      error: null,
      loading: true,
      inviteUser,
    };
  }
  return {
    loading: false,
    inviteUser,
    error,
  };
};
