import { useCallback } from "react";
import { useIdentityProvider } from "../IdentityProvider";
import { RegisterPayload } from "../types/RegisterPayload";
import { useAtom } from "jotai";
import {
  startRegistrationAtom,
  registrationErrorAtom,
} from "../authentication/authAtom";
import { AuthOptions, useHandleUserResult } from "./useHandleUserResult";

export const useRegister = () => {
  const identityProvider = useIdentityProvider();
  const [registrationInProgress, startRegistration] = useAtom(
    startRegistrationAtom
  );
  const [registrationError, setRegistrationError] = useAtom(
    registrationErrorAtom
  );
  const handleUserResult = useHandleUserResult(setRegistrationError);

  const register = useCallback(
    async (payload: RegisterPayload, options?: AuthOptions) => {
      startRegistration();
      const userResult = await identityProvider.register(payload);
      handleUserResult(userResult, options);
    },
    [handleUserResult, identityProvider, startRegistration]
  );

  return { register, registrationInProgress, registrationError };
};
