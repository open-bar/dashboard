import { useAtom } from "jotai";
import { logoutAtom } from "../authentication/authAtom";

export const useLogout = () => {
  const [isAuthenticated, logout] = useAtom(logoutAtom);
  return { isAuthenticated, logout };
};
