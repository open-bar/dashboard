import { useCallback } from "react";
import { useIdentityProvider } from "../IdentityProvider";
import { useAtom } from "jotai";
import { startLoginAtom, loginErrorAtom } from "../authentication/authAtom";
import { AuthOptions, useHandleUserResult } from "./useHandleUserResult";
import { LoginPayload } from "../types";

export const useLogin = () => {
  const identityProvider = useIdentityProvider();
  const [loginInProgress, startLogin] = useAtom(startLoginAtom);
  const [loginError, setLoginError] = useAtom(loginErrorAtom);

  const handleUserResult = useHandleUserResult(setLoginError);

  const login = useCallback(
    async (payload: LoginPayload, options?: AuthOptions) => {
      startLogin();
      const userResult = await identityProvider.login(payload);
      handleUserResult(userResult, options);
    },
    [identityProvider, handleUserResult, startLogin]
  );

  return { login, loginInProgress, loginError };
};
