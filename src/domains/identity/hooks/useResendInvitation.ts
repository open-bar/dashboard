import { useIdentityProvider } from "../IdentityProvider";
import { useCreateSnackbarMessage } from "@/domains/common/snackbar/hooks";
import { useCallback } from "react";
import { InvitedUserI } from "../types";
import { isResultError } from "@/utils";

export const useResendInvitation = (invitedUser: InvitedUserI) => {
  const identityProvider = useIdentityProvider();
  const createSnackbarMessage = useCreateSnackbarMessage();

  return useCallback(async () => {
    const result = await identityProvider.resendInvitation(invitedUser.id);
    if (isResultError(result)) {
      createSnackbarMessage({
        severity: "error",
        message: `Une erreur innattendue est survenue lors du ré-envoi de l'invitation à ${invitedUser.email}`,
      });
    } else {
      createSnackbarMessage({
        severity: "success",
        message: `L'invitation de ${invitedUser.email} a bien été ré-envoyée`,
      });
    }
  }, [identityProvider, invitedUser, createSnackbarMessage]);
};
