import { useCallback, useEffect, useMemo, useState } from "react";
import { resultPipe } from "@/utils/Result";
import { FetchArrayError } from "@/utils/errors";
import { useAtom } from "jotai";
import { HookWithLoading } from "@/utils/HookWithLoading";
import { invitedUsersAtom, usersAtom } from "../atoms";
import { InvitedUserI, UserI } from "../types";
import { useIdentityProvider } from "../IdentityProvider";
import { UserAndInvitedUserI, UserKind } from "../types/UserAndInvited";
import { getResultState } from "@/utils";

export const useUsers = (): HookWithLoading<
  UserAndInvitedUserI[],
  FetchArrayError<UserI | InvitedUserI>
> => {
  const [users, setUsers] = useAtom(usersAtom);
  const [invitedUsers, setInvitedUsers] = useAtom(invitedUsersAtom);
  const [error, setError] = useState<FetchArrayError<
    UserI | InvitedUserI
  > | null>(null);

  const identityProvider = useIdentityProvider();

  const fetchUsers = useCallback(async () => {
    const [usersResult, invitedUsersResult] = await Promise.allSettled([
      identityProvider.getUsers(),
      identityProvider.getInvitedUsers(),
    ]);

    if (usersResult.status === "fulfilled") {
      resultPipe(usersResult.value, setUsers, setError);
    }
    if (invitedUsersResult.status === "fulfilled") {
      resultPipe(invitedUsersResult.value, setInvitedUsers, setError);
    }
  }, [setUsers, setInvitedUsers, identityProvider]);

  useEffect(() => {
    setUsers(null);
    setInvitedUsers(null);
    setError(null);
    fetchUsers();
  }, [fetchUsers, setUsers, setInvitedUsers]);

  const result = useMemo<UserAndInvitedUserI[]>(
    () => [
      ...(users?.map(
        (user): UserAndInvitedUserI => ({
          type: UserKind.ACTUAL_USER,
          value: user,
          id: user.id,
        })
      ) ?? []),
      ...(invitedUsers?.map(
        (user): UserAndInvitedUserI => ({
          type: UserKind.INVITED_USER,
          value: user,
          id: user.id,
        })
      ) ?? []),
    ],
    [users, invitedUsers]
  );

  return getResultState(result, error);
};
