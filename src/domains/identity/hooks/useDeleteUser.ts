import { useAtom } from "jotai";
import { useIdentityProvider } from "../IdentityProvider";
import { addUserAtom, removeUserAtom } from "../atoms";
import { useCreateSnackbarMessage } from "@/domains/common/snackbar/hooks";
import { useCallback } from "react";
import { UserI } from "../types";
import { isResultError } from "@/utils";

export const useDeleteUser = (user: UserI) => {
  const identityProvider = useIdentityProvider();
  const [, removeInvitedUser] = useAtom(removeUserAtom);
  const [, addUser] = useAtom(addUserAtom);
  const createSnackbarMessage = useCreateSnackbarMessage();

  return useCallback(async () => {
    const previousState = removeInvitedUser(user.id);
    const result = await identityProvider.deleteUser(user.id);
    if (isResultError(result)) {
      createSnackbarMessage({
        severity: "error",
        message: `Une erreur innattendue est survenue lors de la suppression de ${user.username}`,
      });
      if (previousState) {
        addUser(user);
      }
    } else {
      createSnackbarMessage({
        severity: "success",
        message: `${user.username} a bien été supprimé·e`,
      });
    }
  }, [
    identityProvider,
    removeInvitedUser,
    user,
    addUser,
    createSnackbarMessage,
  ]);
};
