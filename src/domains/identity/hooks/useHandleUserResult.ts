import { Result, resultPipe } from "@/utils/Result";
import { useCallback } from "react";
import { AuthError, AuthResponseI } from "../types";
import { useAtom } from "jotai";
import {
  currentUserAtom,
  getRedirectToAfterLogin,
} from "../authentication/authAtom";
import { useNavigate } from "react-router-dom";
import { saveAuthResponse } from "../authentication/authStorage";
import { useIdentityProvider } from "../IdentityProvider";

export interface AuthOptions {
  saveCredentials?: boolean;
}

export const useHandleUserResult = (onError: (error: AuthError) => unknown) => {
  const navigate = useNavigate();
  const [, setUser] = useAtom(currentUserAtom);
  const [redirectToAfterLogin] = useAtom(getRedirectToAfterLogin);
  const identityProvider = useIdentityProvider();
  return useCallback(
    (
      userResult: Result<AuthResponseI, AuthError>,
      authOptions: AuthOptions = { saveCredentials: false }
    ) => {
      resultPipe(
        userResult,
        (authResult) => {
          identityProvider.setToken(authResult.token);
          saveAuthResponse(authResult, authOptions);
          setUser(authResult.user);
          navigate(redirectToAfterLogin ?? "/", { replace: true });
        },
        onError
      );
    },
    [onError, navigate, setUser, redirectToAfterLogin, identityProvider]
  );
};
