import { afterEach, describe, test } from "vitest";
import { asAVisitor } from "@/tests/Visitor";

afterEach(() => {});

describe("Login", () => {
  test("User can register and logout from the app and login again", async () => {
    await asAVisitor()
      .visisiting("/catalog/screens")
      .isOnTheLoginPage()
      .then((visitor) => visitor.goToRegistrationPage())
      .then((visitor) =>
        visitor
          .submitsRegistrationForm(
            "ivanLeBG",
            "ivan@qg.fr",
            "amazing and secured password"
          )
          .isOnTheScreensPage()
      )
      .then((visitor) => visitor.disconnectFromTheApp().isOnTheLoginPage())
      .then((visitor) =>
        visitor.submitsLoginForm("ivanLeBG", "amazing and secured password")
      );
  });
});
