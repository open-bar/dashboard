import { afterEach, describe, test } from "vitest";
import { asAVisitor } from "@/tests/Visitor";
import { asARegisteredUser } from "@/tests";

afterEach(() => {});

describe("Users", () => {
  test("Visitor cannot access to screens page", async () => {
    await asAVisitor().visisiting("/users").isOnTheLoginPage();
  });
  test("Registered user can view users", async () => {
    await asARegisteredUser()
      .visisiting("/users")
      .currentPageHasTitle("Utilisateurs");
  });
});
