import {
  Box,
  Button,
  Checkbox,
  CircularProgress,
  Container,
  FormControlLabel,
  Grid,
  TextField,
  Typography,
} from "@mui/material";
import React, { useCallback, useState } from "react";
import { useLogin } from "../hooks";
import { Link } from "react-router-dom";
import { REGISTER, ROUTES } from "@/routes";
import { AuthLayout } from "../components/AuthLayout";
import { AuthErrorMessage } from "../components/AuthErrorMessage";

const Login: React.FC = () => {
  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [saveCredentials, setSaveCredentials] = useState<boolean>(false);

  const { login, loginInProgress, loginError } = useLogin();
  const handleSubmit = useCallback(
    (event: React.FormEvent) => {
      event.preventDefault();
      login(
        {
          password,
          username,
        },
        { saveCredentials }
      );
    },
    [login, username, password, saveCredentials]
  );

  return (
    <AuthLayout title="Connexion">
      <Container>
        <Box
          component="form"
          noValidate
          onSubmit={handleSubmit}
          sx={{ mt: 1, width: "100%" }}
        >
          <TextField
            value={username}
            onChange={(e) => setUsername(e.currentTarget.value)}
            margin="normal"
            required
            fullWidth
            label="Identifiant"
            inputProps={{ "aria-label": "Identifiant" }}
            name="username"
            autoComplete="username"
            autoFocus
          />
          <TextField
            value={password}
            onChange={(e) => setPassword(e.currentTarget.value)}
            margin="normal"
            required
            fullWidth
            name="password"
            label="Mot de passe"
            inputProps={{ "aria-label": "Mot de passe" }}
            type="password"
            autoComplete="current-password"
          />
          <AuthErrorMessage error={loginError} />
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Se souvenir de moi"
            value={saveCredentials}
            onChange={(_event, checked) => setSaveCredentials(checked)}
          />
          <Button
            type="submit"
            fullWidth
            disabled={loginInProgress}
            variant="contained"
            name="connection"
            sx={{ mt: 3, mb: 2 }}
          >
            {loginInProgress ? (
              <CircularProgress
                color="inherit"
                size={24}
                sx={{ float: "right" }}
              />
            ) : (
              "Connexion"
            )}
          </Button>
          <Grid container>
            <Grid item>
              <Link to={ROUTES[REGISTER]}>
                <Typography variant="body2">
                  Pas encore de compte ? Je m'inscris !
                </Typography>
              </Link>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </AuthLayout>
  );
};

export default Login;
