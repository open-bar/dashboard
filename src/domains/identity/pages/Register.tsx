import {
  Box,
  Button,
  Checkbox,
  CircularProgress,
  Container,
  FormControlLabel,
  Grid,
  TextField,
  Typography,
} from "@mui/material";
import React, { useCallback, useState } from "react";
import { useRegister } from "../hooks";
import { AuthLayout } from "../components/AuthLayout";
import { LOGIN, ROUTES } from "@/routes";
import { Link } from "react-router-dom";
import { AuthErrorMessage } from "../components/AuthErrorMessage";

const Register: React.FC = () => {
  const [email, setEmail] = useState<string>("");
  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [saveCredentials, setSaveCredentials] = useState<boolean>(false);

  const { register, registrationError, registrationInProgress } = useRegister();
  const handleSubmit = useCallback(
    (event: React.FormEvent) => {
      event.preventDefault();
      register(
        {
          email,
          password,
          username,
        },
        { saveCredentials }
      );
    },
    [register, email, password, username, saveCredentials]
  );
  return (
    <AuthLayout title="Inscription">
      <Container>
        <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 1 }}>
          <TextField
            value={email}
            onChange={(e) => setEmail(e.currentTarget.value)}
            margin="normal"
            required
            fullWidth
            label="Adresse e-mail"
            inputProps={{ "aria-label": "Adresse e-mail" }}
            name="email"
            autoComplete="email"
            autoFocus
          />
          <TextField
            value={username}
            onChange={(e) => setUsername(e.currentTarget.value)}
            margin="normal"
            required
            fullWidth
            label="Identifiant"
            inputProps={{ "aria-label": "Identifiant" }}
            name="username"
            autoComplete="username"
          />
          <TextField
            value={password}
            onChange={(e) => setPassword(e.currentTarget.value)}
            margin="normal"
            required
            fullWidth
            name="password"
            label="Mot de passe"
            inputProps={{ "aria-label": "Mot de passe" }}
            type="password"
            autoComplete="new-password"
          />
          <AuthErrorMessage error={registrationError} />
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Se souvenir de moi"
            value={saveCredentials}
            onChange={(_event, checked) => setSaveCredentials(checked)}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            name="registration"
            disabled={registrationInProgress}
            sx={{ mt: 3, mb: 2 }}
          >
            {registrationInProgress ? (
              <CircularProgress
                color="inherit"
                size={24}
                sx={{ float: "right" }}
              />
            ) : (
              "Je m'inscris"
            )}
          </Button>
          <Grid container>
            <Grid item>
              <Link to={ROUTES[LOGIN]}>
                <Typography variant="body2">
                  Déjà un compte ? Je me connecte !
                </Typography>
              </Link>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </AuthLayout>
  );
};

export default Register;
