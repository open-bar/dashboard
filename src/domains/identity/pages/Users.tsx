import { MainTitle } from "@/components/MainTitle";
import { Add } from "@mui/icons-material";
import { Alert, Button, Container } from "@mui/material";
import React, { useState } from "react";
import { Loader } from "@/components/Loader";
import { genericErrorMessage } from "@/utils/errors";
import { useUsers } from "../hooks/useUsers";
import { UsersList } from "../components/UsersList";
import { ModalInviteUser } from "../components/ModalInviteUser";

const Users: React.FC = () => {
  const { result: users, loading, error } = useUsers();
  const [createModalVisible, setCreateModalVisible] = useState<boolean>(false);
  return (
    <Container>
      <MainTitle title={"Utilisateurs"}>
        <Button
          variant="outlined"
          startIcon={<Add />}
          onClick={() => setCreateModalVisible(true)}
        >
          Ajouter
        </Button>
      </MainTitle>
      {loading && <Loader />}
      {error && (
        <Alert severity="error">{genericErrorMessage[error.type]}</Alert>
      )}
      {users && <UsersList users={users} />}
      <ModalInviteUser
        open={createModalVisible}
        close={() => setCreateModalVisible(false)}
      />
    </Container>
  );
};

export default Users;
