import React, { ReactNode } from "react";
import { Alert } from "@mui/material";
import { AuthError, AuthErrorType } from "../types";
import { FetchErrorType, genericErrorMessage } from "@/utils/errors";

interface Props {
  error?: AuthError;
}

const messages: Record<AuthErrorType, ReactNode> = {
  [AuthErrorType.INVALID_LOGIN_OR_PASSWORD]:
    "Identifiant ou mot de passe invalide.",
  [AuthErrorType.UNKNOWN_ERROR]: "Une erreur inattendue est servenue.",
};

export const AuthErrorMessage: React.FC<Props> = ({ error }) => {
  if (!error) {
    return null;
  }
  if (error.type === FetchErrorType.CUSTOM_ERROR) {
    return <Alert severity="error">{messages[error.error]}</Alert>;
  }
  return <Alert severity="error">{genericErrorMessage[error.type]}</Alert>;
};
