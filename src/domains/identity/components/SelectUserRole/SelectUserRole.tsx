import React, { useCallback } from "react";
import { UserRole } from "../../types/UserRole";
import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
} from "@mui/material";
import { parseUserRole } from "../../types/parseUserRole";
import { Role } from "../Role";

interface Props {
  value: UserRole;
  onChange: (value: UserRole) => void;
}

export const SelectUserRole: React.FC<Props> = ({ value, onChange }) => {
  const handleChange = useCallback(
    (event: SelectChangeEvent<UserRole>) => {
      const userRole = parseUserRole(event.target.value);
      onChange(userRole);
    },
    [onChange]
  );
  return (
    <FormControl fullWidth variant="standard" margin="normal">
      <InputLabel>Rôle</InputLabel>
      <Select value={value} label="Rôle" onChange={handleChange}>
        <MenuItem value={UserRole.ADMIN}>
          <Role role={UserRole.ADMIN} />
        </MenuItem>
        <MenuItem value={UserRole.ACCOUTANT}>
          <Role role={UserRole.ACCOUTANT} />
        </MenuItem>
        <MenuItem value={UserRole.POINT_OF_SALE}>
          <Role role={UserRole.POINT_OF_SALE} />
        </MenuItem>
        <MenuItem value={UserRole.SUPPLY_MANAGER}>
          <Role role={UserRole.SUPPLY_MANAGER} />
        </MenuItem>
        <MenuItem value={UserRole.NONE}>
          <Role role={UserRole.NONE} />
        </MenuItem>
        <MenuItem value={20}>Twenty</MenuItem>
        <MenuItem value={30}>Thirty</MenuItem>
      </Select>
    </FormControl>
  );
};
