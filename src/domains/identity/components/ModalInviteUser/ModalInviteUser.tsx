import {
  Alert,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
} from "@mui/material";
import React, { useCallback, useState } from "react";
import { Loader } from "@/components/Loader";
import { UserRole } from "../../types/UserRole";
import { useInviteUser } from "../../hooks";
import { SelectUserRole } from "../SelectUserRole";

interface Props {
  open: boolean;
  close: () => void;
}

export const ModalInviteUser: React.FC<Props> = ({ open, close }) => {
  const [email, setEmail] = useState<string>("");
  const [role, setRole] = useState<UserRole>(UserRole.NONE);

  const { inviteUser, loading, error } = useInviteUser();

  const onSubmit = useCallback(
    async (event: React.FormEvent<HTMLFormElement>) => {
      event.preventDefault();
      await inviteUser({ email, role });
      setEmail("");
      close();
    },
    [email, role, close, inviteUser]
  );
  return (
    <Dialog
      open={open}
      onClose={close}
      PaperProps={{
        component: "form",
        onSubmit: onSubmit,
      }}
    >
      <DialogTitle>Inviter un·e utilisateur·trice</DialogTitle>
      {loading ? (
        <DialogContent>
          <Loader />
        </DialogContent>
      ) : (
        <DialogContent>
          <TextField
            autoFocus
            value={email}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              setEmail(event.currentTarget.value);
            }}
            margin="normal"
            required
            name="email"
            label="E-mail de l'utilateur·trice"
            inputProps={{ "aria-label": "Email de l'utilisateur·trice" }}
            type="email"
            fullWidth
            variant="standard"
          />
          <SelectUserRole value={role} onChange={setRole} />
          {error && <Alert severity="error">{error.type}</Alert>}
        </DialogContent>
      )}
      <DialogActions>
        <Button onClick={close}>Annuler</Button>
        <Button type="submit">Créer</Button>
      </DialogActions>
    </Dialog>
  );
};
