import React from "react";
import { DataTable, DataTableColumn } from "@/components/DataTable";
import {
  ActualUserKind,
  UserAndInvitedUserI,
  UserKind,
} from "../../types/UserAndInvited";
import { Role } from "../Role";
import { UserActionMenu } from "./UserActionMenu";

interface Props {
  users: UserAndInvitedUserI[];
}

const isActualUser = (user: UserAndInvitedUserI): user is ActualUserKind => {
  return user.type === UserKind.ACTUAL_USER;
};

const getUserLabel = (user: UserAndInvitedUserI) =>
  isActualUser(user) ? user.value.username : user.value.email;

const getUserRole = (user: UserAndInvitedUserI) => user.value.role;

const getUserStatus = (user: UserAndInvitedUserI) =>
  isActualUser(user) ? "Actif·ve" : "Invitation envoyée";

const columns: DataTableColumn<UserAndInvitedUserI>[] = [
  {
    id: "name",
    label: "Nom",
    getValue: getUserLabel,
    width: 100,
    flexGrow: 1,
    flexShrink: 1,
  },
  {
    id: "role",
    label: "Rôle",
    width: 150,
    getValue: getUserRole,
    renderCell: ({ value }) => <Role role={value.role} />,
  },
  {
    id: "status",
    label: "Statut",
    width: 150,
    getValue: getUserStatus,
  },
  {
    id: "action",
    label: "",
    width: 40,
    key: "id",
    renderCell: (user) => <UserActionMenu user={user} />,
  },
];

export const UsersList: React.FC<Props> = ({ users }) => {
  return <DataTable data={users} columns={columns} />;
};
