import React from "react";
import { InvitedUserI } from "../../types";
import { useCancelInvitation } from "../../hooks/useCancelInvitation";
import { MenuItem } from "@mui/material";
import { useResendInvitation } from "../../hooks";

interface Props {
  invitedUser: InvitedUserI;
}

export const UserInviteActionMenu: React.FC<Props> = ({ invitedUser }) => {
  const cancelInvitation = useCancelInvitation(invitedUser);
  const resendInvitation = useResendInvitation(invitedUser);
  return (
    <>
      <MenuItem onClick={cancelInvitation}>Annuler l'invitation</MenuItem>
      <MenuItem onClick={resendInvitation}>Ré-envoyer l'invitation</MenuItem>
    </>
  );
};
