import React from "react";
import { UserI } from "../../types";
import { MenuItem } from "@mui/material";
import { useDeleteUser } from "../../hooks";
import { UserRole } from "../../types/UserRole";

interface Props {
  user: UserI;
}

export const ActualUserActionMenu: React.FC<Props> = ({ user }) => {
  const deleteUser = useDeleteUser(user);
  if (user.role === UserRole.OWNER) {
    return null;
  }
  return (
    <>
      {/*<MenuItem onClick={cancelInvitation}>Transmettre la propriété</MenuItem>*/}
      <MenuItem onClick={deleteUser}>Supprimer</MenuItem>
    </>
  );
};
