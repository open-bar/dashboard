import React, { MouseEvent, useCallback, useState } from "react";
import { UserAndInvitedUserI, UserKind } from "../../types/UserAndInvited";
import { IconButton, Menu } from "@mui/material";
import { MoreVertRounded } from "@mui/icons-material";
import { stopPropagation } from "@/utils";
import { UserInviteActionMenu } from "./UserInviteActionMenu";
import { ActualUserActionMenu } from "./ActualUserActionMenu";
import { UserRole } from "../../types/UserRole";

interface Props {
  user: UserAndInvitedUserI;
}

export const UserActionMenu: React.FC<Props> = ({ user }) => {
  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);

  const toggleMenu = useCallback((event: MouseEvent<HTMLButtonElement>) => {
    event.stopPropagation();
    setAnchorEl((anchor) => (anchor ? null : event.currentTarget));
  }, []);
  const closeMenu = useCallback(() => setAnchorEl(null), []);

  if (
    user.type === UserKind.ACTUAL_USER &&
    user.value.role === UserRole.OWNER
  ) {
    return null;
  }

  return (
    <>
      <IconButton onClick={toggleMenu} onMouseDown={stopPropagation}>
        <MoreVertRounded />
      </IconButton>
      <Menu
        anchorEl={anchorEl}
        open={!!anchorEl}
        onClose={closeMenu}
        onMouseDown={stopPropagation}
        onClick={closeMenu}
        anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
      >
        {user.type === UserKind.INVITED_USER ? (
          <UserInviteActionMenu invitedUser={user.value} />
        ) : (
          <ActualUserActionMenu user={user.value} />
        )}
      </Menu>
    </>
  );
};
