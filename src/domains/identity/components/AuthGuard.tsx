import React, { useEffect } from "react";
import { generatePath, useLocation, useNavigate } from "react-router-dom";
import { LOGIN, ROUTES } from "@/routes";
import { Layout } from "@/components/Layout";
import { redirectToAfterLoginAtom } from "../authentication/authAtom";
import { useAtom } from "jotai";

export const AuthGuard: React.FC = () => {
  const [isAuth, setRedirectToAfterLogin] = useAtom(redirectToAfterLoginAtom);
  const location = useLocation();
  const navigate = useNavigate();

  useEffect(() => {
    if (!isAuth) {
      setRedirectToAfterLogin(
        `${location.pathname}${location.search}${location.hash}`
      );
      navigate(generatePath(ROUTES[LOGIN]), { replace: true });
    }
  }, [isAuth, navigate, location, setRedirectToAfterLogin]);

  if (!isAuth) {
    return null;
  }
  return <Layout />;
};
