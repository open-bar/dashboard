import React from "react";
import { UserRole } from "../types/UserRole";

interface Props {
  role: UserRole;
}

const roleLabel: Record<UserRole, string> = {
  [UserRole.OWNER]: "Propriétaire",
  [UserRole.ADMIN]: "Administrateur·trice",
  [UserRole.ACCOUTANT]: "Comptable",
  [UserRole.SUPPLY_MANAGER]: "Approvisionnement",
  [UserRole.POINT_OF_SALE]: "Vendeur·se",
  [UserRole.NONE]: "Aucun",
};

export const Role: React.FC<Props> = ({ role }) => roleLabel[role];
