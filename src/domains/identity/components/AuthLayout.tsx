import { Copyleft } from "@/components/Copyleft";
import { LockOutlined } from "@mui/icons-material";
import { Avatar, Box, Grid, Paper, Typography } from "@mui/material";
import React, { PropsWithChildren, ReactNode } from "react";

interface Props extends PropsWithChildren {
  title: ReactNode;
}

export const AuthLayout: React.FC<Props> = ({ title, children }) => (
  <Grid container component="main" sx={{ height: "100vh" }}>
    <Grid
      item
      xs={false}
      sm={4}
      md={7}
      sx={{
        backgroundImage: "url(https://source.unsplash.com/random?wallpapers)",
        backgroundRepeat: "no-repeat",
        backgroundColor: (t) =>
          t.palette.mode === "light" ? t.palette.grey[50] : t.palette.grey[900],
        backgroundSize: "cover",
        backgroundPosition: "center",
      }}
    />
    <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
      <Box
        sx={{
          maxWidth: 400,
          my: 8,
          mx: "auto",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
          <LockOutlined />
        </Avatar>
        <Typography component="h1" variant="h5" role="heading" aria-level={1}>
          {title}
        </Typography>
        {children}
        <Copyleft sx={{ mt: 5 }} />
      </Box>
    </Grid>
  </Grid>
);
