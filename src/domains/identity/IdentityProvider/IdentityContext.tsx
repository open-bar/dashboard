import { PropsWithChildren, createContext, useEffect, useState } from "react";

const identityProviderType = import.meta.env.VITE_IDENTITY_PROVIDER;
import {} from "./IdentityProvider.api";
import {} from "./IdentityProvider.inMemory";
import { IdentityProvider } from "./IdentityProviderI";
import { ProviderType } from "../types";
import { getSavedAuthResponse } from "../authentication/authStorage";
import { currentUserAtom } from "../authentication/authAtom";
import { useAtom } from "jotai";
import { Loading } from "@/Loading";
const IdentityProviderModule =
  identityProviderType === ProviderType.ACTUAL
    ? import("./IdentityProvider.api")
    : import("./IdentityProvider.inMemory");

const identityBaseUrl = import.meta.env.VITE_IDENTITY_BASE_URL;

export const IdentityContext = createContext<IdentityProvider | null>(null);

interface Props extends PropsWithChildren {}

export const IdentityProviderContext: React.FC<Props> = ({ children }) => {
  const [provider, setProvider] = useState<IdentityProvider | null>(null);
  const [, setUser] = useAtom(currentUserAtom);
  useEffect(() => {
    const getProvider = async () => {
      const IdentityProvider = (await IdentityProviderModule).default;
      const identityProvider = new IdentityProvider(identityBaseUrl);
      const savedAuthResponse = getSavedAuthResponse();
      if (savedAuthResponse) {
        identityProvider.setToken(savedAuthResponse.token);
        setUser(savedAuthResponse.user);
      }
      setProvider(identityProvider);
    };
    getProvider();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  if (!provider) {
    return <Loading />;
  }
  return (
    <IdentityContext.Provider value={provider}>
      {children}
    </IdentityContext.Provider>
  );
};
