import { http, HttpResponse } from "msw";
import { setupServer } from "msw/node";
import {
  afterAll,
  afterEach,
  assert,
  beforeAll,
  describe,
  expect,
  it,
} from "vitest";
import IndentityProviderApi from "./IdentityProvider.api";
import { v4 } from "uuid";
import {
  AuthErrorType,
  AuthResponseI,
  LoginPayload,
  RegisterPayload,
  UserId,
} from "../types";
import { isResultError } from "@/utils/Result";
import { FetchErrorType } from "@/utils/errors";
import { UserRole } from "../types/UserRole";

const baseUrl = "http://localhost:8080/";

const identityProvider = new IndentityProviderApi(baseUrl);

const registeredUsers: AuthResponseI[] = [
  {
    token: "token-1",
    user: {
      username: "test-user-1",
      id: "f30f9496-8bdb-45ec-95b4-bed7041a33a1" as UserId,
      role: UserRole.OWNER,
    },
  },
];
const userPasswords: Record<string, string> = {
  "test-user-1": "test-password-1",
};

const server = setupServer(
  http.post(`${baseUrl}register`, async ({ request }) => {
    const data = (await request.json()) as RegisterPayload;
    const user = registeredUsers.find((u) => u.user.username === data.username);
    if (user) {
      if (userPasswords[user.user.username] === data.password) {
        return HttpResponse.json(user);
      }
      return HttpResponse.json(
        {
          error: AuthErrorType.INVALID_LOGIN_OR_PASSWORD,
        },
        { status: 400 }
      );
    }
    const response: AuthResponseI = {
      token: v4(),
      user: {
        id: v4() as UserId,
        username: data.username,
        role: UserRole.NONE,
      },
    };
    registeredUsers.push(response);
    return HttpResponse.json(response);
  }),

  http.post(`${baseUrl}login`, async ({ request }) => {
    const data = (await request.json()) as LoginPayload;
    const user = registeredUsers.find((u) => u.user.username === data.username);
    if (!user || userPasswords[user.user.username] !== data.password) {
      return HttpResponse.json(
        {
          error: AuthErrorType.INVALID_LOGIN_OR_PASSWORD,
        },
        { status: 400 }
      );
    }
    return HttpResponse.json(user);
  })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe("IdentityProvider API", () => {
  it("returns token and user when login is successful", async () => {
    const response = await identityProvider.login({
      username: "test-user-1",
      password: "test-password-1",
    });
    expect(response.success).toBeTruthy();
    expect(response.value?.token).toBeDefined();

    expect(response.value?.user).toMatchObject({
      id: "f30f9496-8bdb-45ec-95b4-bed7041a33a1",
      username: "test-user-1",
    });
  });

  it("return an invalid email or password error if user does not exist", async () => {
    const response = await identityProvider.login({
      username: "test-bad-user-1",
      password: "test-password-1",
    });
    expect(response.success).toBeFalsy();
    assert(isResultError(response));
    expect(response.error.type).toEqual(FetchErrorType.CUSTOM_ERROR);
    assert(response.error.type === FetchErrorType.CUSTOM_ERROR);
    expect(response.error.error).toEqual(
      AuthErrorType.INVALID_LOGIN_OR_PASSWORD
    );
  });

  it("return an invalid email or password error if user exists but password is incorrect", async () => {
    const response = await identityProvider.login({
      username: "test-user-1",
      password: "test-bad-password-1",
    });
    expect(response.success).toBeFalsy();
    assert(isResultError(response));
    expect(response.error.type).toEqual(FetchErrorType.CUSTOM_ERROR);
    assert(response.error.type === FetchErrorType.CUSTOM_ERROR);
    expect(response.error.error).toEqual(
      AuthErrorType.INVALID_LOGIN_OR_PASSWORD
    );
  });

  it("returns token and user when register is successful", async () => {
    const response = await identityProvider.register({
      username: "test-user-2",
      password: "test-password-2",
      email: "test@test.tot",
    });
    expect(response.success).toBeTruthy();
    expect(response.value?.token).toBeDefined();
    expect(response.value?.user).toMatchObject({
      username: "test-user-2",
    });
  });

  it("returns an invalid email or password error if user try to register with existing username", async () => {
    const response = await identityProvider.register({
      username: "test-user-2",
      password: "test-password-2",
      email: "test@test.tot",
    });
    expect(response.success).toBeFalsy();
    assert(isResultError(response));
    expect(response.error.type).toEqual(FetchErrorType.CUSTOM_ERROR);
    assert(response.error.type === FetchErrorType.CUSTOM_ERROR);
    expect(response.error.error).toEqual(
      AuthErrorType.INVALID_LOGIN_OR_PASSWORD
    );
  });
});
