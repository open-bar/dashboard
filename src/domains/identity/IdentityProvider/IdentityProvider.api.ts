import * as Sentry from "@sentry/browser";
import {
  Result,
  ResultError,
  ResultSuccess,
  getResultErrorValue,
  getResultSuccessValue,
  isResultError,
  isResultSuccess,
} from "@/utils/Result";
import {
  InviteUserPayload,
  InvitedUserI,
  InvitedUserId,
  LoginPayload,
  RegisterPayload,
  UserI,
  UserId,
  parseAuthError,
  parseAuthResponse,
  parseInvitedUser,
  parseUser,
} from "../types";
import { IdentityProvider } from "./IdentityProviderI";
import {
  FetchArrayError,
  FetchError,
  FetchErrorType,
  ParsingErrorType,
} from "@/utils/errors";

export class IndentityProviderApi extends IdentityProvider {
  constructor(private readonly baseUrl: string) {
    super();
  }

  async parseResponse<T, ErrorType = FetchError>(
    response: Response,
    parser: (data: unknown) => Result<T, ParsingErrorType>,
    errorParser?: (data: unknown) => Result<ErrorType, ParsingErrorType>
  ): Promise<Result<T, ErrorType | FetchError>> {
    if (errorParser && response.status >= 400 && response.status < 500) {
      const parsedErrorResult = errorParser(await response.json());
      if (isResultSuccess(parsedErrorResult)) {
        return ResultError(parsedErrorResult.value);
      }
    }
    if (response.status < 200 || response.status >= 300) {
      return ResultError<FetchError>({
        type: FetchErrorType.HTTP_REQUEST_ERROR,
        code: response.status,
        error: `${response.text}`,
      });
    }
    const result = parser(await response.json());
    if (isResultError(result)) {
      return ResultError({
        type: FetchErrorType.PARSING_ERROR,
        error: getResultErrorValue(result),
      });
    }
    return result;
  }

  async parseResponseAsArray<T, ErrorType = FetchArrayError<T>>(
    response: Response,
    parser: (data: unknown) => Result<T, ParsingErrorType>,
    errorParser?: (data: unknown) => Result<ErrorType, ParsingErrorType>
  ): Promise<Result<T[], ErrorType | FetchArrayError<T>>> {
    if (errorParser && response.status >= 400 && response.status < 500) {
      const parsedErrorResult = errorParser(await response.json());
      if (isResultSuccess(parsedErrorResult)) {
        return ResultError(parsedErrorResult.value);
      }
    }
    if (response.status < 200 || response.status >= 300) {
      return ResultError({
        type: FetchErrorType.HTTP_REQUEST_ERROR,
        code: response.status,
        error: `${response.text}`,
      });
    }
    const data = await response.json();
    if (!Array.isArray(data)) {
      const error: FetchArrayError<T> = {
        type: FetchErrorType.PARSING_ERROR,
        partialResults: [],
        errors: [
          {
            data,
            errorsKey: [],
          },
        ],
      };

      Sentry.captureException(error, { extra: { url: response.url } });
      return ResultError(error);
    }
    const results = data.map(parser);
    const successResult = results.filter(isResultSuccess);
    if (successResult.length !== results.length) {
      const errors = results.filter(isResultError);
      return ResultError({
        type: FetchErrorType.PARSING_ERROR,
        partialResults: successResult.map(getResultSuccessValue),
        errors: errors.map(getResultErrorValue),
      });
    }
    return ResultSuccess(successResult.map(getResultSuccessValue));
  }

  request(url: string, method: string = "GET", body?: unknown) {
    return fetch(new URL(url, this.baseUrl).toString(), {
      method: method,
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    });
  }

  post(url: string, body?: unknown) {
    return this.request(url, "POST", body);
  }

  async register(payload: RegisterPayload) {
    const response = await this.post("register", payload);
    return await this.parseResponse(
      response,
      parseAuthResponse,
      parseAuthError
    );
  }

  async login(payload: LoginPayload) {
    const response = await this.post("login", payload);
    return await this.parseResponse(
      response,
      parseAuthResponse,
      parseAuthError
    );
  }

  async getUsers(): Promise<Result<UserI[], FetchArrayError<UserI>>> {
    const response = await this.request("users");
    return await this.parseResponseAsArray(response, parseUser);
  }

  async deleteUser(userId: UserId): Promise<Result<boolean, FetchError>> {
    const response = await this.request(`users/${userId}`, "DELETE");
    return ResultSuccess(
      response.status >= 200 &&
        response.status < 300 &&
        (await response.text()) === "true"
    );
  }

  async getInvitedUsers(): Promise<
    Result<InvitedUserI[], FetchArrayError<InvitedUserI>>
  > {
    const response = await this.request("users/invite");
    return await this.parseResponseAsArray(response, parseInvitedUser);
  }

  async inviteUser(
    payload: InviteUserPayload
  ): Promise<Result<InvitedUserI, FetchError>> {
    const response = await this.post("users/invite", payload);
    return await this.parseResponse(response, parseInvitedUser);
  }

  async cancelInvitation(
    invitedUserId: InvitedUserId
  ): Promise<Result<boolean, FetchError>> {
    const response = await this.request(
      `users/invite/${invitedUserId}`,
      "DELETE"
    );
    return ResultSuccess(
      response.status >= 200 &&
        response.status < 300 &&
        (await response.text()) === "true"
    );
  }

  async resendInvitation(
    invitedUserId: InvitedUserId
  ): Promise<Result<boolean, FetchError>> {
    const response = await this.post(
      `users/invite/${invitedUserId}/resend-invitation`
    );
    return ResultSuccess(
      response.status >= 200 &&
        response.status < 300 &&
        (await response.text()) === "true"
    );
  }
}
export default IndentityProviderApi;
