import { useContext } from "react";
import { IdentityContext } from "./IdentityContext";
import { IdentityProvider } from "./IdentityProviderI";

export const useIdentityProvider = (): IdentityProvider => {
  const identityProvider = useContext(IdentityContext);
  if (identityProvider === null) {
    throw new Error(
      "useIdentityProvider cannot be used outside of IdentityProvider context"
    );
  }
  return identityProvider;
};
