import { Result, ResultError, ResultSuccess } from "@/utils/Result";
import {
  AuthError,
  AuthErrorType,
  InviteUserPayload,
  InvitedUserI,
  InvitedUserId,
  LoginPayload,
  RegisterPayload,
  UserI,
  UserId,
} from "../types";
import { IdentityProvider } from "./IdentityProviderI";
import { v4 } from "uuid";
import { FetchArrayError, FetchError, FetchErrorType } from "@/utils/errors";
import { UserRole } from "../types/UserRole";

const isInTest = import.meta.env.MODE === "test";
const sleepDuration = isInTest ? 0 : 500;

export const mockedRegisteredUser: RegisterPayload & UserI = {
  id: v4() as UserId,
  username: "test-1",
  email: "test-1@yopmail.com",
  password: "testtest",
  role: UserRole.OWNER,
};

const mockedUsers: UserI[] = [
  {
    id: mockedRegisteredUser.id,
    username: mockedRegisteredUser.username,
    role: mockedRegisteredUser.role,
  },
];

const sleep = (time: number) =>
  new Promise<void>((resolve) => {
    setTimeout(resolve, time);
  });

export class IdentityProviderInMemory extends IdentityProvider {
  private users: UserI[];
  private userPasswords: Record<UserId, string>;
  private invitedUsers: InvitedUserI[];

  constructor() {
    super();
    this.users = JSON.parse(JSON.stringify(mockedUsers)) as UserI[];
    this.userPasswords = {
      [mockedRegisteredUser.id]: mockedRegisteredUser.password,
    };
    this.invitedUsers = [
      {
        id: v4() as InvitedUserId,
        email: "toto@toto.to",
        role: UserRole.ACCOUTANT,
      },
    ];
  }

  public async register(payload: RegisterPayload) {
    const user: UserI = {
      id: v4() as UserId,
      username: payload.username,
      role: UserRole.NONE,
    };
    this.userPasswords[user.id] = payload.password;
    this.users.push(user);
    await sleep(sleepDuration);
    return ResultSuccess({ token: v4(), user });
  }

  async login(payload: LoginPayload) {
    const user = this.users.find((u) => u.username === payload.username);
    await sleep(sleepDuration);
    if (!user || payload.password !== this.userPasswords[user.id]) {
      return ResultError<AuthError>({
        type: FetchErrorType.CUSTOM_ERROR,
        error: AuthErrorType.INVALID_LOGIN_OR_PASSWORD,
      });
    }
    return ResultSuccess({ token: v4(), user });
  }

  async getUsers(): Promise<Result<UserI[], FetchArrayError<UserI>>> {
    return ResultSuccess(this.users);
  }

  async deleteUser(userId: UserId): Promise<Result<boolean, FetchError>> {
    const newUsers = this.users.filter((u) => u.id !== userId);
    if (newUsers.length !== this.invitedUsers.length) {
      this.users = newUsers;
      return ResultSuccess(true);
    }
    return ResultSuccess(false);
  }

  async getInvitedUsers(): Promise<
    Result<InvitedUserI[], FetchArrayError<InvitedUserI>>
  > {
    return ResultSuccess(this.invitedUsers);
  }

  async inviteUser(
    payload: InviteUserPayload
  ): Promise<Result<InvitedUserI, FetchError>> {
    const invitedUser: InvitedUserI = {
      ...payload,
      id: v4() as InvitedUserId,
    };
    this.invitedUsers = [...this.invitedUsers, invitedUser];
    return ResultSuccess(invitedUser);
  }

  async cancelInvitation(
    invitedUserId: InvitedUserId
  ): Promise<Result<boolean, FetchError>> {
    const newInvitedUsers = this.invitedUsers.filter(
      (u) => u.id !== invitedUserId
    );
    if (newInvitedUsers.length !== this.invitedUsers.length) {
      this.invitedUsers = newInvitedUsers;
      return ResultSuccess(true);
    }
    return ResultSuccess(false);
  }

  async resendInvitation(
    invitedUserId: InvitedUserId
  ): Promise<Result<boolean, FetchError>> {
    const invitedUsers = this.invitedUsers.find((u) => u.id === invitedUserId);
    if (invitedUsers) {
      return ResultSuccess(true);
    }
    return ResultSuccess(false);
  }
}
export default IdentityProviderInMemory;
