import { Result } from "@/utils/Result";
import {
  AuthError,
  AuthResponseI,
  InvitedUserI,
  InvitedUserId,
  LoginPayload,
  RegisterPayload,
  UserI,
  UserId,
} from "../types";
import { FetchArrayError, FetchError } from "@/utils/errors";
import { InviteUserPayload } from "../types/InviteUserPayload";

export abstract class IdentityProvider {
  private authToken: string | null = null;
  private subscribers: ((token: string | null) => void)[] = [];

  setToken(token: string | null) {
    this.authToken = token;
    this.subscribers.forEach((subscriber) => subscriber(token));
  }

  getToken() {
    return this.authToken;
  }

  subscribe(callback: (token: string | null) => void) {
    this.subscribers.push(callback);
  }
  unsubscribe(callback: (token: string | null) => void) {
    this.subscribers = this.subscribers.filter(
      (subscriber) => subscriber !== callback
    );
  }

  public abstract register(
    payload: RegisterPayload
  ): Promise<Result<AuthResponseI, AuthError>>;
  public abstract login(
    payload: LoginPayload
  ): Promise<Result<AuthResponseI, AuthError>>;

  public abstract getUsers(): Promise<Result<UserI[], FetchArrayError<UserI>>>;

  public abstract deleteUser(
    userId: UserId
  ): Promise<Result<boolean, FetchError>>;

  public abstract getInvitedUsers(): Promise<
    Result<InvitedUserI[], FetchArrayError<InvitedUserI>>
  >;
  public abstract inviteUser(
    payload: InviteUserPayload
  ): Promise<Result<InvitedUserI, FetchError>>;
  public abstract cancelInvitation(
    invitedUserId: InvitedUserId
  ): Promise<Result<boolean, FetchError>>;
  public abstract resendInvitation(
    invitedUserId: InvitedUserId
  ): Promise<Result<boolean, FetchError>>;
}
