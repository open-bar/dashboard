import React from "react";
import { SnackbarMessageI } from "../snackbar.types";
import {
  Alert,
  Slide,
  SlideProps,
  Snackbar,
  SnackbarOrigin,
  styled,
} from "@mui/material";
import { useDeleteSnackbarMessage } from "../hooks/useDeleteSnackbarMessage";

interface Props {
  message: SnackbarMessageI;
}

const defaultOrigin: SnackbarOrigin = {
  vertical: "top",
  horizontal: "right",
};

const StyledSnackbar = styled(Snackbar)({
  marginTop: "64px",
});

const getKeyFromOrigin = (origin: SnackbarOrigin) =>
  `${origin.vertical}-${origin.horizontal}`;

function SlideTransition(props: SlideProps) {
  return <Slide {...props} direction="left" />;
}

export const SnackbarMesage: React.FC<Props> = ({ message }) => {
  const { deleteSnackbar, open } = useDeleteSnackbarMessage(message);
  const anchorOrigin = message.anchorOrigin || defaultOrigin;
  return (
    <StyledSnackbar
      anchorOrigin={anchorOrigin}
      open={open}
      onClose={deleteSnackbar}
      key={getKeyFromOrigin(anchorOrigin)}
      autoHideDuration={message.autoHideDuration ?? 5000}
      TransitionComponent={SlideTransition}
    >
      <Alert
        onClose={deleteSnackbar}
        severity={message.severity}
        sx={{ width: "100%" }}
      >
        {message.message}
      </Alert>
    </StyledSnackbar>
  );
};
