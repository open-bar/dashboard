import React from "react";
import { useSnackbarMessages } from "../hooks";
import { SnackbarMessageI } from "../snackbar.types";
import { SnackbarMesage } from "./SnackbarMessage";

const renderSnackBarMessage = (message: SnackbarMessageI) => (
  <SnackbarMesage message={message} key={message.id} />
);

export const SnackBarRenderer: React.FC = () => {
  const messages = useSnackbarMessages();
  return <>{messages.map(renderSnackBarMessage)}</>;
};
