import { AlertColor, SnackbarOrigin } from "@mui/material";

export interface CreateSnackbarMessagePayload {
  anchorOrigin?: SnackbarOrigin;
  message: string;
  severity?: AlertColor;
  key?: string;
  autoHideDuration?: number;
}

export interface SnackbarMessageI extends CreateSnackbarMessagePayload {
  id: string;
  severity: AlertColor;
}
