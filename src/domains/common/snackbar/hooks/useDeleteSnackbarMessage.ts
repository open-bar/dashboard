import { useAtom } from "jotai";
import { snackbarState } from "../snackbar.atom";
import { useCallback, useState } from "react";
import { SnackbarMessageI } from "../snackbar.types";

export const useDeleteSnackbarMessage = (message: SnackbarMessageI) => {
  const [open, setOpen] = useState(true);
  const [, setSnackbarMessages] = useAtom(snackbarState);
  const deleteSnackbar = useCallback(() => {
    setOpen(false);
    window.setTimeout(() => {
      setSnackbarMessages((prev) => prev.filter((m) => m.id !== message.id));
    }, 1000);
  }, [setSnackbarMessages, message]);

  return { deleteSnackbar, open };
};
