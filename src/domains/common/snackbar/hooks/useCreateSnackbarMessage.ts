import { useAtom } from "jotai";
import { snackbarState } from "../snackbar.atom";
import { useCallback } from "react";
import { v4 } from "uuid";
import { CreateSnackbarMessagePayload } from "../snackbar.types";

export const useCreateSnackbarMessage = () => {
  const [, setSnackbarMessages] = useAtom(snackbarState);
  return useCallback(
    (payload: CreateSnackbarMessagePayload) => {
      setSnackbarMessages((prev) => [
        ...prev,
        { severity: "info", ...payload, id: v4() },
      ]);
    },
    [setSnackbarMessages]
  );
};
