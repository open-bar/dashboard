import { useAtom } from "jotai";
import { snackbarState } from "../snackbar.atom";

export const useSnackbarMessages = () => {
  const [messages] = useAtom(snackbarState);
  return messages;
};
