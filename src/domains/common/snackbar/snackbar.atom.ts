import { atom } from "jotai";
import { SnackbarMessageI } from "./snackbar.types";

type SnackbarState = SnackbarMessageI[];

export const snackbarState = atom<SnackbarState>([]);
