import { useCallback, useState } from "react";

export const useBooleanState = (defaultValue = false) => {
  const [value, setValue] = useState<boolean>(defaultValue);
  const setTrue = useCallback(() => setValue(true), []);
  const setFalse = useCallback(() => setValue(false), []);
  const toggle = useCallback(() => setValue((current) => !current), []);
  return { value, setTrue, setFalse, toggle };
};
