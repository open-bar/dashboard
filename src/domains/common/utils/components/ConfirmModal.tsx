import { Loader } from "@/components/Loader";
import {
  Alert,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Typography,
} from "@mui/material";
import {
  PropsWithChildren,
  ReactNode,
  memo,
  useCallback,
  useState,
} from "react";

interface Props extends PropsWithChildren {
  open: boolean;
  close: () => void;
  onConfirm: () => Promise<boolean> | void | boolean;
  failedMessage?: ReactNode;
  title: ReactNode;
}
export const ConfirmModal = memo<Props>(
  ({ open, close, onConfirm, title, failedMessage, children }: Props) => {
    const [error, setError] = useState<boolean>();
    const [loading, setLoading] = useState<boolean>(false);
    const confirmAction = useCallback(async () => {
      try {
        const result = onConfirm();
        if (result instanceof Promise) {
          setLoading(true);
          const awaitedResult = await result;
          if (awaitedResult === false) {
            setError(true);
          } else {
            close();
          }
        } else if (result === false) {
          setError(true);
        } else {
          close();
        }
      } catch (e) {
        setError(true);
      } finally {
        setLoading(false);
      }
    }, [onConfirm, close]);

    return (
      <Dialog
        open={open}
        onClose={close}
        PaperProps={{
          component: "form",
          onSubmit: confirmAction,
        }}
      >
        <DialogTitle>{title}</DialogTitle>
        {loading ? (
          <DialogContent>
            <Loader />
          </DialogContent>
        ) : (
          <DialogContent>
            <Typography>{children}</Typography>
            {error && <Alert severity="error">{failedMessage}</Alert>}
          </DialogContent>
        )}
        <DialogActions>
          <Button onClick={close}>Annuler</Button>
          <Button type="submit" color="error">
            Confirmer
          </Button>
        </DialogActions>
      </Dialog>
    );
  },
  (prevProps, nextProps) =>
    prevProps.children === nextProps.children &&
    prevProps.title === nextProps.title &&
    prevProps.open === nextProps.open &&
    prevProps.close === nextProps.close &&
    prevProps.failedMessage === nextProps.failedMessage &&
    prevProps.onConfirm === nextProps.onConfirm
);

ConfirmModal.displayName = "ConfirmModal";
