import { useCallback, useState } from "react";
import { useCatalogProvider } from "../providers/useCatalogProvider";
import { ResultError, resultPipe } from "@/utils/Result";
import { FetchError } from "@/utils/errors";
import { useAtom } from "jotai";
import { productsAtom } from "../atoms";
import { ProductInCreationI } from "../types";

export const useCreateProduct = () => {
  const [, setProducts] = useAtom(productsAtom);
  const [error, setError] = useState<FetchError | null>(null);
  const [loading, setLoading] = useState<boolean>(false);

  const catalogProvider = useCatalogProvider();

  const createProduct = useCallback(
    async (payload: ProductInCreationI) => {
      if (!payload.accountingGroup) {
        return ResultError(null);
      }
      setLoading(true);
      const result = await catalogProvider.createProduct({
        accountingGroupId: payload.accountingGroup.id,
        name: payload.name,
        shortName: payload.shortName,
        purchasePrice: payload.purchasePrice,
        sellPrice: payload.sellPrice,
        image: payload.image,
        freePrice: payload.freePrice,
      });
      resultPipe(
        result,
        (product) => {
          setProducts((products) => [...(products ?? []), product]);
        },
        setError
      );
      setLoading(false);
      return result;
    },
    [catalogProvider, setProducts]
  );

  if (error) {
    return {
      error,
      loading: false,
      createProduct,
    };
  } else if (loading) {
    return {
      error: null,
      loading: true,
      createProduct,
    };
  }
  return {
    loading: false,
    createProduct,
    error,
  };
};
