import { useEffect, useState } from "react";
import { useCatalogProvider } from "../providers/useCatalogProvider";
import { resultPipe } from "@/utils/Result";
import { FetchArrayError } from "@/utils/errors";
import { useAtom } from "jotai";
import { AccountingGroupI } from "../types/AccountingGroup";
import { HookWithLoading } from "@/utils/HookWithLoading";
import { accountingGroupsAtom } from "../atoms/accountingGroupsAtom";
import { getResultState } from "@/utils";

export const useAccountingGroups = (): HookWithLoading<
  AccountingGroupI[],
  FetchArrayError<AccountingGroupI>
> => {
  const [accoutingGroups, setAccountingGroups] = useAtom(accountingGroupsAtom);
  const [error, setError] = useState<FetchArrayError<AccountingGroupI> | null>(
    null
  );

  const catalogProvider = useCatalogProvider();

  useEffect(() => {
    setAccountingGroups(null);
    setError(null);
    catalogProvider.getAccountingGroups().then((result) => {
      resultPipe(result, setAccountingGroups, setError);
    });
  }, [catalogProvider, setAccountingGroups]);

  return getResultState(accoutingGroups, error);
};
