import { useEffect, useMemo, useState } from "react";
import { FetchError } from "@/utils/errors";
import { useAtom } from "jotai";
import { HookWithLoading } from "@/utils/HookWithLoading";
import { ProductI, ProductId } from "../types/Product";
import { productsAtom } from "../atoms";
import { useCatalogProvider } from "../providers/useCatalogProvider";
import { resultPipe } from "@/utils/Result";
import { getResultState } from "@/utils";

export const useProduct = (productId: ProductId): HookWithLoading<ProductI> => {
  const [products, setProducts] = useAtom(productsAtom);
  const product = useMemo(
    () => products?.find((product) => product.id === productId),
    [products, productId]
  );
  const [error, setError] = useState<FetchError | null>(null);

  const catalogProvider = useCatalogProvider();

  useEffect(() => {
    if (!product) {
      catalogProvider.getProductById(productId).then((result) => {
        resultPipe(
          result,
          (product) =>
            setProducts((products) => [...(products ?? []), product]),
          setError
        );
      });
    }
  }, [product, setProducts, catalogProvider, productId]);

  return getResultState(product, error);
};
