import { useCallback } from "react";
import { useCatalogProvider } from "../providers/useCatalogProvider";
import { ScreenI, ScreenId } from "../types/Screen";
import { useAtom } from "jotai";
import { updateScreenAtom } from "../atoms";
import { isResultError } from "@/utils/Result";
import { useCreateSnackbarMessage } from "@/domains/common/snackbar/hooks";

export const useUpdateScreen = (screenId: ScreenId) => {
  const catalogProvider = useCatalogProvider();
  const [, updateScreen] = useAtom(updateScreenAtom);
  const createSnackbarMessage = useCreateSnackbarMessage();

  return useCallback(
    async (updatePayload: Partial<Omit<ScreenI, "id">>) => {
      const previousState = updateScreen(screenId, updatePayload);
      const result = await catalogProvider.updateScreen(
        screenId,
        updatePayload
      );
      if (isResultError(result)) {
        createSnackbarMessage({
          severity: "error",
          message:
            "Une erreur innattendue est survenue lors de la mise à jour de l'écran",
        });
        if (previousState) {
          updateScreen(screenId, previousState);
        }
      }
    },
    [catalogProvider, screenId, updateScreen, createSnackbarMessage]
  );
};
