import { useAtom } from "jotai";
import { productsFiltersAtom } from "../atoms";
import { useAccountingGroups } from "./useAccountingGroups";
import { useCallback } from "react";
import { AccountingGroupI } from "../types";

export const useProductsAccountingGroupFilter = (): [
  AccountingGroupI | null,
  (accountingGroup: AccountingGroupI | null) => void
] => {
  const [productsFilter, setProductsFilter] = useAtom(productsFiltersAtom);
  const { result: accountingGroups } = useAccountingGroups();
  const accountingGroup = productsFilter.accoutingGroupId
    ? accountingGroups?.find(
        (accGroup) => accGroup.id === productsFilter.accoutingGroupId
      ) ?? null
    : null;

  const setAccountingGroup = useCallback(
    (accountingGroup: AccountingGroupI | null) => {
      setProductsFilter((state) => ({
        ...state,
        accoutingGroupId: accountingGroup ? accountingGroup.id : null,
      }));
    },
    [setProductsFilter]
  );

  return [accountingGroup, setAccountingGroup];
};
