import { useAtom } from "jotai";
import { productsFiltersAtom } from "../atoms";
import { ChangeEvent, useCallback } from "react";

export const useProductsFilterQuery = (): [
  string,
  (event: string | ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => void
] => {
  const [productsFilter, setProductsFilter] = useAtom(productsFiltersAtom);
  const setQuery = useCallback(
    (event: string | ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
      setProductsFilter((state) => ({
        ...state,
        query: typeof event === "string" ? event : event.currentTarget.value,
      }));
    },
    [setProductsFilter]
  );
  return [productsFilter.query, setQuery];
};
