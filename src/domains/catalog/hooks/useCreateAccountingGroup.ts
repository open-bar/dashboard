import { useCallback, useState } from "react";
import { useCatalogProvider } from "../providers/useCatalogProvider";
import { resultPipe } from "@/utils/Result";
import { FetchError } from "@/utils/errors";
import { useAtom } from "jotai";
import { CreateAccountingGroupPayload } from "../types/AccountingGroup";
import { accountingGroupsAtom } from "../atoms/accountingGroupsAtom";

export const useCreateAccountingGroup = () => {
  const [, setAccountingGroups] = useAtom(accountingGroupsAtom);
  const [error, setError] = useState<FetchError | null>(null);
  const [loading, setLoading] = useState<boolean>(false);

  const catalogProvider = useCatalogProvider();

  const createAccountingGroup = useCallback(
    async (payload: CreateAccountingGroupPayload) => {
      setLoading(true);
      const result = await catalogProvider.createAccountingGroup(payload);
      resultPipe(
        result,
        (accountingGroup) => {
          setAccountingGroups((accountingGroups) => [
            ...(accountingGroups ?? []),
            accountingGroup,
          ]);
        },
        setError
      );
      setLoading(false);
      return result;
    },
    [catalogProvider, setAccountingGroups]
  );

  if (error) {
    return {
      error,
      loading: false,
      createAccountingGroup,
    };
  } else if (loading) {
    return {
      error: null,
      loading: true,
      createAccountingGroup,
    };
  }
  return {
    loading: false,
    createAccountingGroup,
    error,
  };
};
