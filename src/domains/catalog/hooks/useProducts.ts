import { useEffect, useMemo, useState } from "react";
import { useCatalogProvider } from "../providers/useCatalogProvider";
import { useAtom } from "jotai";
import { ProductI } from "../types/Product";
import { productsAtom, productsFiltersAtom } from "../atoms";
import {
  FetchArrayError,
  getResultState,
  getTextFilterWithRank,
  getValueFromRanked,
  HookWithLoading,
  rankedResultSorter,
  resultIsRanked,
  resultPipe,
} from "@/utils";

export const useProducts = (): HookWithLoading<
  ProductI[],
  FetchArrayError<ProductI>
> => {
  const [products, setProducts] = useAtom(productsAtom);
  const [productsFilters] = useAtom(productsFiltersAtom);
  const [error, setError] = useState<FetchArrayError<ProductI> | null>(null);

  const catalogProvider = useCatalogProvider();

  const filteredProducts = useMemo(() => {
    if (!products) return products;
    const productsByGroup = productsFilters.accoutingGroupId
      ? products.filter(
          (product) =>
            product.accountingGroup.id === productsFilters.accoutingGroupId
        )
      : products;
    if (productsFilters.query.trim().length === 0) return productsByGroup;
    return productsByGroup
      .map(
        getTextFilterWithRank(productsFilters.query, [
          "name",
          (product) => product.accountingGroup.name,
        ])
      )
      .filter(resultIsRanked)
      .sort(rankedResultSorter)
      .map(getValueFromRanked);
  }, [products, productsFilters]);

  useEffect(() => {
    setProducts(null);
    setError(null);
    catalogProvider.getProducts().then((result) => {
      resultPipe(result, setProducts, setError);
    });
  }, [catalogProvider, setProducts]);

  return getResultState(filteredProducts, error);
};
