import { useEffect, useMemo, useState } from "react";
import { FetchError } from "@/utils/errors";
import { useAtom } from "jotai";
import { HookWithLoading } from "@/utils/HookWithLoading";
import { screensAtom } from "../atoms";
import { useCatalogProvider } from "../providers/useCatalogProvider";
import { resultPipe } from "@/utils/Result";
import { getResultState } from "@/utils";
import { ScreenI, ScreenId } from "../types";

export const useScreen = (screenId: ScreenId): HookWithLoading<ScreenI> => {
  const [screens, setScreens] = useAtom(screensAtom);
  const screen = useMemo(
    () => screens?.find((screen) => screen.id === screenId),
    [screens, screenId]
  );
  const [error, setError] = useState<FetchError | null>(null);

  const catalogProvider = useCatalogProvider();

  useEffect(() => {
    if (!screen) {
      catalogProvider.getScreenById(screenId).then((result) => {
        resultPipe(
          result,
          (screen) => setScreens((screens) => [...(screens ?? []), screen]),
          setError
        );
      });
    }
  }, [screen, setScreens, catalogProvider, screenId]);

  return getResultState(screen, error);
};
