import { useEffect, useState } from "react";
import { useCatalogProvider } from "../providers/useCatalogProvider";
import { ScreenI } from "../types/Screen";
import { useAtom } from "jotai";
import { screensAtom } from "../atoms";
import {
  FetchArrayError,
  getResultState,
  HookWithLoading,
  resultPipe,
} from "@/utils";

export const useScreens = (): HookWithLoading<
  ScreenI[],
  FetchArrayError<ScreenI>
> => {
  const [screens, setScreens] = useAtom(screensAtom);
  const [error, setError] = useState<FetchArrayError<ScreenI> | null>(null);

  const catalogProvider = useCatalogProvider();

  useEffect(() => {
    setScreens(null);
    setError(null);
    catalogProvider.getScreens().then((result) => {
      resultPipe(result, setScreens, setError);
    });
  }, [catalogProvider, setScreens]);

  return getResultState(screens, error);
};
