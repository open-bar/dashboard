import { useCallback } from "react";
import { useCatalogProvider } from "../providers/useCatalogProvider";
import { useAtom } from "jotai";
import { updateProductAtom } from "../atoms";
import { isResultError } from "@/utils/Result";
import { useCreateSnackbarMessage } from "@/domains/common/snackbar/hooks";
import { ProductI, ProductId } from "../types";

export const useUpdateProduct = (productId: ProductId) => {
  const catalogProvider = useCatalogProvider();
  const [, updateProduct] = useAtom(updateProductAtom);
  const createSnackbarMessage = useCreateSnackbarMessage();

  return useCallback(
    async (updatePayload: Partial<Omit<ProductI, "id">>) => {
      const previousState = updateProduct(productId, updatePayload);
      const result = await catalogProvider.updateProduct(
        productId,
        updatePayload
      );
      if (isResultError(result)) {
        createSnackbarMessage({
          severity: "error",
          message:
            "Une erreur innattendue est survenue lors de la mise à jour du produit",
        });
        if (previousState) {
          updateProduct(productId, previousState);
        }
      }
    },
    [catalogProvider, productId, updateProduct, createSnackbarMessage]
  );
};
