import { useCallback } from "react";
import { useCatalogProvider } from "../providers/useCatalogProvider";
import { useAtom } from "jotai";
import { addProductAtom, deleteProductAtom } from "../atoms";
import { isResultError } from "@/utils/Result";
import { useCreateSnackbarMessage } from "@/domains/common/snackbar/hooks";
import { ProductId } from "../types";

export const useDeleteProduct = (productId: ProductId) => {
  const catalogProvider = useCatalogProvider();
  const [, deleteProduct] = useAtom(deleteProductAtom);
  const [, addProduct] = useAtom(addProductAtom);
  const createSnackbarMessage = useCreateSnackbarMessage();

  return useCallback(async () => {
    const previousState = deleteProduct(productId);
    const result = await catalogProvider.deleteProduct(productId);
    if (isResultError(result)) {
      createSnackbarMessage({
        severity: "error",
        message:
          "Une erreur innattendue est survenue lors de l'archivage du produit",
      });
      if (previousState) {
        addProduct(previousState);
      }
      return false;
    }
    return true;
  }, [
    catalogProvider,
    productId,
    deleteProduct,
    addProduct,
    createSnackbarMessage,
  ]);
};
