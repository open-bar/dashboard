import { AmountI } from "../types/Product";

export const getAmountValue = (amount: AmountI) => amount / 100_000;

export const renderAmount = (amount: AmountI) =>
  `${getAmountValue(amount).toLocaleString(undefined, {
    style: "currency",
    currency: "EUR",
  })}`;
