import { VatRateI } from "../types/AccountingGroup";

export const getVatRateValue = (vatRate: VatRateI) => vatRate / 100_000;

export const renderVatRate = (vatRate: VatRateI) =>
  `${getVatRateValue(vatRate).toLocaleString(undefined, {
    style: "percent",
    minimumFractionDigits: 0,
    maximumFractionDigits: 2,
  })}`;
