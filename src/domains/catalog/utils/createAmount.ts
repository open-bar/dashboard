import { AmountI } from "../types/Product";

export const createAmount = (value: number): AmountI =>
  Math.round(value * 100_000) as AmountI;
