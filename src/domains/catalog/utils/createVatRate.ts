import { VatRateI } from "../types/AccountingGroup";

export const createVatRate = (value: number): VatRateI =>
  Math.round(value * 1_000) as VatRateI;
