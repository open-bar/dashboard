import { ProductI, ScreenI } from "../types";

export const isChildScreen = (child: ScreenI | ProductI): child is ScreenI =>
  Array.isArray(child.children);
