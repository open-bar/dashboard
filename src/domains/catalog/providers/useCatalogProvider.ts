import { useContext } from "react";
import { CatalogProviderI } from "./CatalogProvider";
import { CatalogContext } from "./CatalogContext";

export const useCatalogProvider = (): CatalogProviderI => {
  const catalogProvider = useContext(CatalogContext);
  if (catalogProvider === null) {
    throw new Error(
      "useCatalogProvider cannot be used outside of CatalogProvider Context"
    );
  }
  return catalogProvider;
};
