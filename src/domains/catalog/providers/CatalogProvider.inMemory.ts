import {
  Result,
  ResultError,
  ResultSuccess,
  isResultSuccess,
} from "@/utils/Result";
import { FetchArrayError, FetchError, FetchErrorType } from "@/utils/errors";
import { ScreenI, ScreenId } from "../types/Screen";
import { CatalogProviderI } from "./CatalogProvider";
import { v4 } from "uuid";
import {
  AccountingGroupI,
  AccountingGroupId,
  CreateAccountingGroupPayload,
  VatRateI,
} from "../types/AccountingGroup";
import { CreateProductPayload, ProductI, ProductId } from "../types/Product";
import { createAmount } from "../utils";

export const mockedAccountingGroups: AccountingGroupI[] = [
  {
    id: v4() as AccountingGroupId,
    name: "Bières",
    vatRate: 20000 as VatRateI,
  },
  {
    id: v4() as AccountingGroupId,
    name: "Vins",
    vatRate: 20000 as VatRateI,
  },
  {
    id: v4() as AccountingGroupId,
    name: "Soft",
    vatRate: 10000 as VatRateI,
  },
  {
    id: v4() as AccountingGroupId,
    name: "Boissons chaudes",
    vatRate: 10000 as VatRateI,
  },
  {
    id: v4() as AccountingGroupId,
    name: "Dons",
    vatRate: 0 as VatRateI,
  },
];

const mockedProducts: ProductI[] = [
  {
    id: "ebdc0243-9398-4419-8f18-3e4b6e1ded16" as ProductId,
    name: "Pinte de blonde",
    accountingGroup: mockedAccountingGroups[0],
    purchasePrice: createAmount(2.5),
    sellPrice: createAmount(6),
    image: null,
    freePrice: false,
  },
  {
    id: v4() as ProductId,
    name: "Demi de blonde",
    accountingGroup: mockedAccountingGroups[0],
    purchasePrice: createAmount(1.25),
    sellPrice: createAmount(3.5),
    image: null,
    freePrice: false,
  },
  {
    id: v4() as ProductId,
    name: "Don",
    accountingGroup: mockedAccountingGroups[4],
    purchasePrice: createAmount(0),
    sellPrice: null,
    image: null,
    freePrice: true,
  },
];

const mockedScreens: ScreenI[] = [
  {
    id: v4() as ScreenId,
    name: "Bières",
    order: "1",
    active: true,
    color: "#FFFF00",
    children: [
      {
        id: v4() as ScreenId,
        name: "Blonde",
        order: "1",
        active: true,
        children: [mockedProducts[1], mockedProducts[0]],
        color: "#ffffb8",
      },
      {
        id: v4() as ScreenId,
        name: "IPA",
        order: "2",
        active: true,
        children: [],
        color: "#ffeead",
      },
      {
        id: v4() as ScreenId,
        name: "Blanche",
        order: "3",
        active: true,
        children: [],
        color: "#fffacf",
      },
      {
        id: v4() as ScreenId,
        name: "Ambrée",
        order: "4",
        active: true,
        children: [],
        color: "#d3ad63",
      },
      {
        id: v4() as ScreenId,
        name: "Stout",
        order: "5",
        active: true,
        children: [],
        color: "#5f533b",
      },
      {
        id: v4() as ScreenId,
        name: "Sans alcool",
        order: "6",
        active: true,
        children: [],
        color: "#ffffa8",
      },
    ],
  },
  {
    id: v4() as ScreenId,
    name: "Vins",
    order: "2",
    active: true,
    color: "#FF0000",
    children: [],
  },
  {
    id: v4() as ScreenId,
    name: "Soft",
    order: "3",
    active: true,
    color: "#FF00FF",
    children: [],
  },
];

class CatalogProviderInMemory implements CatalogProviderI {
  private authToken: string | null = null;
  private _screens: ScreenI[];
  private _accountingGroups: AccountingGroupI[];
  private _products: ProductI[];

  constructor() {
    this._screens = JSON.parse(JSON.stringify(mockedScreens)) as ScreenI[];
    this._accountingGroups = JSON.parse(
      JSON.stringify(mockedAccountingGroups)
    ) as AccountingGroupI[];
    this._products = JSON.parse(JSON.stringify(mockedProducts)) as ProductI[];
  }

  setToken(token: string | null) {
    this.authToken = token;
  }

  async getScreens(): Promise<Result<ScreenI[], FetchArrayError<ScreenI>>> {
    if (!this.authToken) {
      return ResultError({
        type: FetchErrorType.UNAUTHORIZED,
      });
    }
    return ResultSuccess(this._screens);
  }

  async getScreenById(
    screenId: ScreenId
  ): Promise<Result<ScreenI, FetchError>> {
    if (!this.authToken) {
      return ResultError({
        type: FetchErrorType.UNAUTHORIZED,
      });
    }
    const screen = this._screens.find((screen) => screen.id === screenId);
    if (!screen) {
      return ResultError({
        type: FetchErrorType.HTTP_REQUEST_ERROR,
        code: 404,
        error: "Not found",
      });
    }
    return ResultSuccess(screen);
  }

  async updateScreen(
    screenId: ScreenId,
    payload: Partial<Omit<ScreenI, "id">>
  ): Promise<Result<ScreenI, FetchError>> {
    const screenIndex = this._screens.findIndex(
      (screen) => screen.id === screenId
    );
    const screen = {
      ...this._screens[screenIndex],
      ...payload,
    };
    this._screens = [
      ...this._screens.slice(0, screenIndex),
      screen,
      ...this._screens.slice(screenIndex + 1),
    ];
    return ResultSuccess(screen);
  }

  async getAccountingGroups(): Promise<
    Result<AccountingGroupI[], FetchArrayError<AccountingGroupI>>
  > {
    if (!this.authToken) {
      return ResultError({
        type: FetchErrorType.UNAUTHORIZED,
      });
    }
    return ResultSuccess(this._accountingGroups);
  }

  async createAccountingGroup(
    payload: CreateAccountingGroupPayload
  ): Promise<Result<AccountingGroupI, FetchError>> {
    const accountingGroup: AccountingGroupI = {
      id: v4() as AccountingGroupId,
      ...payload,
    };
    this._accountingGroups = [...this._accountingGroups, accountingGroup];
    return ResultSuccess(accountingGroup);
  }

  async getProducts(): Promise<Result<ProductI[], FetchArrayError<ProductI>>> {
    if (!this.authToken) {
      return ResultError({
        type: FetchErrorType.UNAUTHORIZED,
      });
    }
    return ResultSuccess(this._products);
  }

  async getProductById(
    productId: ProductId
  ): Promise<Result<ProductI, FetchError>> {
    if (!this.authToken) {
      return ResultError({
        type: FetchErrorType.UNAUTHORIZED,
      });
    }
    const product = this._products.find((product) => product.id === productId);
    if (!product) {
      return ResultError({
        type: FetchErrorType.HTTP_REQUEST_ERROR,
        code: 404,
        error: "Not found",
      });
    }
    return ResultSuccess(product);
  }

  async createProduct(
    payload: CreateProductPayload
  ): Promise<Result<ProductI, FetchError>> {
    const { accountingGroupId, ...productFields } = payload;
    const accountingGroup = this._accountingGroups.find(
      (accountingGroup) => accountingGroup.id === accountingGroupId
    );
    if (!accountingGroup) {
      return ResultError({
        type: FetchErrorType.HTTP_REQUEST_ERROR,
        code: 400,
        error: "Bad Request",
      });
    }
    const product: ProductI = {
      id: v4() as ProductId,
      ...productFields,
      accountingGroup,
    };
    this._products = [...this._products, product];
    return ResultSuccess(product);
  }

  async updateProduct(
    productId: ProductId,
    payload: Partial<Omit<ProductI, "id">>
  ): Promise<Result<ProductI, FetchError>> {
    const result = await this.getProductById(productId);
    if (!isResultSuccess(result)) {
      return result;
    }
    const product: ProductI = { ...result.value, ...payload };
    this._products = this._products.map((p) =>
      p.id === productId ? product : p
    );
    return ResultSuccess(product);
  }

  async deleteProduct(
    productId: ProductId
  ): Promise<Result<boolean, FetchError>> {
    const result = await this.getProductById(productId);
    if (!isResultSuccess(result)) {
      return result;
    }
    this._products = this._products.filter((p) => p.id !== productId);
    return ResultSuccess(true);
  }
}

export default CatalogProviderInMemory;
