import React from "react";
import { CatalogProviderContext } from "./CatalogContext";
import { Outlet } from "react-router-dom";

export const WithCatalogProvider: React.FC = () => (
  <CatalogProviderContext>
    <Outlet />
  </CatalogProviderContext>
);

export default WithCatalogProvider;
