import { PropsWithChildren, createContext, useEffect, useState } from "react";
import { Loading } from "@/Loading";
import { CatalogProviderI } from "./CatalogProvider";
import { useIdentityProvider } from "@/domains/identity";

const CatalogProviderModule = import("./CatalogProvider.inMemory");

export const CatalogContext = createContext<CatalogProviderI | null>(null);

export const CatalogProviderContext: React.FC<PropsWithChildren> = ({
  children,
}) => {
  const identityProvider = useIdentityProvider();
  const [provider, setProvider] = useState<CatalogProviderI | null>(null);

  useEffect(() => {
    const getProvider = async () => {
      const CatalogProvider = (await CatalogProviderModule).default;
      const catalogProvider = new CatalogProvider();
      catalogProvider.setToken(identityProvider.getToken());
      setProvider(catalogProvider);
    };
    getProvider();
  }, [identityProvider]);

  useEffect(() => {
    if (provider) {
      const onTokenChange = (token: string | null) => {
        provider.setToken(token);
      };
      identityProvider.subscribe(onTokenChange);
      return () => identityProvider.unsubscribe(onTokenChange);
    }
  }, [identityProvider, provider]);

  if (!provider) {
    return <Loading />;
  }
  return (
    <CatalogContext.Provider value={provider}>
      {children}
    </CatalogContext.Provider>
  );
};
