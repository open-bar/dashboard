import { Result } from "@/utils/Result";
import { ScreenI, ScreenId } from "../types/Screen";
import { FetchArrayError, FetchError } from "@/utils/errors";
import {
  AccountingGroupI,
  CreateAccountingGroupPayload,
} from "../types/AccountingGroup";
import { CreateProductPayload, ProductI, ProductId } from "../types/Product";

export interface CatalogProviderI {
  setToken: (token: string | null) => void;

  getScreens: () => Promise<Result<ScreenI[], FetchArrayError<ScreenI>>>;
  getScreenById: (screenId: ScreenId) => Promise<Result<ScreenI, FetchError>>;
  updateScreen: (
    screenId: ScreenId,
    payload: Partial<Omit<ScreenI, "id">>
  ) => Promise<Result<ScreenI, FetchError>>;

  getAccountingGroups: () => Promise<
    Result<AccountingGroupI[], FetchArrayError<AccountingGroupI>>
  >;
  createAccountingGroup: (
    payload: CreateAccountingGroupPayload
  ) => Promise<Result<AccountingGroupI, FetchError>>;

  createProduct: (
    payload: CreateProductPayload
  ) => Promise<Result<ProductI, FetchError>>;
  getProducts: () => Promise<Result<ProductI[], FetchArrayError<ProductI>>>;
  getProductById: (
    productId: ProductId
  ) => Promise<Result<ProductI, FetchError>>;
  updateProduct: (
    productId: ProductId,
    payload: Partial<Omit<ProductI, "id">>
  ) => Promise<Result<ProductI, FetchError>>;
  deleteProduct: (productId: ProductId) => Promise<Result<boolean, FetchError>>;
}
