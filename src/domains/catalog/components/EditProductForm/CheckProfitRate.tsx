import React from "react";
import { AmountI, ProductInCreationI, VatRateI } from "../../types";
import { createVatRate, getVatRateValue } from "../../utils";
import { isDefined } from "@/utils/isDefined";
import { Alert, AlertColor } from "@mui/material";
import { Amount } from "../Amount";
import { VatRate } from "../VatRate";

interface Props {
  product: ProductInCreationI;
}

const getSeverityColor = (profitRate: VatRateI | null): AlertColor => {
  if (!isDefined(profitRate)) return "info";
  if (profitRate < 0) return "error";
  if (profitRate > createVatRate(60)) return "success";
  if (profitRate < createVatRate(40)) return "warning";
  return "info";
};

export const CheckProfitRate: React.FC<Props> = ({ product }) => {
  const { sellPrice, accountingGroup, purchasePrice } = product;

  const sellPriceWithoutTax =
    sellPrice && accountingGroup
      ? ((sellPrice /
          (1 + getVatRateValue(accountingGroup.vatRate))) as AmountI)
      : null;

  const netProfit =
    sellPriceWithoutTax && isDefined(purchasePrice)
      ? ((sellPriceWithoutTax - purchasePrice) as AmountI)
      : null;

  const profitRate =
    netProfit && sellPriceWithoutTax
      ? createVatRate((100 * netProfit) / sellPriceWithoutTax)
      : null;

  const profitInfoColor = getSeverityColor(profitRate);

  if (!netProfit || !profitRate) return null;

  return (
    <Alert severity={profitInfoColor}>
      Marge NET : <Amount value={netProfit} variant="body2" /> (
      <VatRate vatRate={profitRate} variant="body2" />)
    </Alert>
  );
};
