import { Checkbox, FormControlLabel, TextField } from "@mui/material";
import Grid from "@mui/material/Unstable_Grid2"; // Grid version 2
import React, { ChangeEvent, useCallback } from "react";
import { SelectAccountingGroup } from "../../components/SelectAccountingGroup";
import { AccountingGroupI, AmountI, ProductInCreationI } from "../../types";
import { AmountField } from "../../components/AmountField";
import { CheckProfitRate } from "./CheckProfitRate";

interface Props {
  product: ProductInCreationI;
  onChange: (product: Partial<ProductInCreationI>) => void;
}

export const EditProductForm: React.FC<Props> = ({ product, onChange }) => {
  const { name, shortName, accountingGroup, purchasePrice, sellPrice } =
    product;

  const onAccountingGroupChange = useCallback(
    (accountingGroup: AccountingGroupI | null) => {
      onChange({ accountingGroup });
    },
    [onChange]
  );

  const onNameChange = useCallback(
    (event: ChangeEvent<HTMLInputElement>) => {
      onChange({ name: event.currentTarget.value });
    },
    [onChange]
  );

  const onShortNameChange = useCallback(
    (event: ChangeEvent<HTMLInputElement>) => {
      onChange({ shortName: event.currentTarget.value });
    },
    [onChange]
  );

  const onSellPriceChange = useCallback(
    (sellPrice: AmountI) => {
      onChange({ sellPrice });
    },
    [onChange]
  );

  const onPurchasePriceChange = useCallback(
    (purchasePrice: AmountI) => {
      onChange({ purchasePrice });
    },
    [onChange]
  );

  const onFreePriceChange = useCallback(
    (_event: React.SyntheticEvent, checked: boolean) => {
      onChange({ freePrice: checked });
    },
    [onChange]
  );

  return (
    <Grid container spacing={2}>
      <Grid xs={12}>
        <SelectAccountingGroup
          value={accountingGroup}
          onChange={onAccountingGroupChange}
          required
        />
      </Grid>
      <Grid xs={12} sm={6}>
        <TextField
          value={name}
          onChange={onNameChange}
          margin="none"
          required
          fullWidth
          name="productName"
          label="Nom du produit"
          inputProps={{ "aria-label": "Nom du produit" }}
          type="text"
          autoComplete="off"
        />
      </Grid>
      <Grid xs={12} sm={6}>
        <TextField
          value={shortName}
          onChange={onShortNameChange}
          margin="none"
          fullWidth
          name="productShortName"
          label="Nom court"
          placeholder="Nom affiché sur l'application de caisse (par défaut, le nom du produit est affiché)"
          inputProps={{
            "aria-label":
              "Nom court du produit pour l'affichage sur l'application de caisse",
          }}
          type="text"
          autoComplete="off"
        />
      </Grid>
      <Grid xs={12} sm={6}>
        <AmountField
          value={sellPrice}
          onChange={onSellPriceChange}
          name="productSellPrice"
          label={
            product.freePrice === true
              ? "Prix de vente minimum TTC"
              : "Prix de vente TTC"
          }
        />
      </Grid>
      <Grid xs={12} sm={6}>
        <FormControlLabel
          control={<Checkbox />}
          label="Prix libre"
          value={product.freePrice === true}
          onChange={onFreePriceChange}
        />
      </Grid>
      <Grid xs={12} sm={6}>
        <AmountField
          value={purchasePrice}
          onChange={onPurchasePriceChange}
          name="productPurchasePrice"
          label="Prix d'achat HT"
        />
      </Grid>
      <Grid xs={12}>
        <CheckProfitRate product={product} />
      </Grid>
    </Grid>
  );
};
