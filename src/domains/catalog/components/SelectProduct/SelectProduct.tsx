import React, { HTMLAttributes, useCallback } from "react";
import { ProductI } from "../../types";
import {
  Autocomplete,
  AutocompleteValue,
  Box,
  SxProps,
  TextField,
  TextFieldVariants,
} from "@mui/material";
import { useProducts } from "../../hooks";
import { genericErrorMessage, isDefined } from "@/utils";

interface Props {
  value: ProductI | null;
  onChange: (product: ProductI | null) => void;
  required?: boolean;
  variant?: TextFieldVariants;
  sx?: SxProps;
  margin?: "normal" | "dense" | "none" | undefined;
}

const areProductsEqual = (product1: ProductI, product2: ProductI) =>
  product1.id === product2.id;

const getOptionLabel = (product: ProductI) => `${product.name}`;

const listProps: HTMLAttributes<HTMLUListElement> = {
  "data-testid": "select-product-options",
} as HTMLAttributes<HTMLUListElement>;

export const SelectProduct: React.FC<Props> = ({
  value,
  onChange,
  required,
  variant,
  sx,
  margin,
}) => {
  const { result: products, loading, error } = useProducts();
  const handleChange = useCallback(
    (
      _event: React.SyntheticEvent,
      newValue: AutocompleteValue<ProductI | null, false, false, false>
    ) => {
      onChange(newValue);
    },
    [onChange]
  );
  return (
    <Autocomplete
      data-testid="select-product"
      value={value}
      onChange={handleChange}
      renderInput={(params) => (
        <TextField
          {...params}
          label="Produit"
          error={!!error}
          variant={variant}
          margin={margin}
          helperText={error ? genericErrorMessage[error.type] : null}
          required={required}
          inputProps={{
            ...params.inputProps,
            required: required && isDefined(value),
          }}
        />
      )}
      loading={loading}
      loadingText="Chargement des groupes comptables..."
      options={products ?? []}
      isOptionEqualToValue={areProductsEqual}
      getOptionLabel={getOptionLabel}
      renderOption={(props, option) => (
        <Box component="li" {...props} key={option.id}>
          {getOptionLabel(option)}
        </Box>
      )}
      noOptionsText="Aucun groupe comptable correspondant"
      ListboxProps={listProps}
      sx={sx}
    />
  );
};
