import React, { useCallback } from "react";
import { AccountingGroupI } from "../../types/AccountingGroup";
import { DataTable, DataTableColumn } from "@/components/DataTable";
import { VatRate } from "../VatRate";
import { generatePath, useNavigate } from "react-router-dom";
import { PRODUCTS, ROUTES } from "@/routes";
import { useAtom } from "jotai";
import { productsFiltersAtom } from "../../atoms";

interface Props {
  accountingGroups: AccountingGroupI[];
}

const columns: DataTableColumn<AccountingGroupI>[] = [
  {
    id: "name",
    label: "Nom",
    key: "name",
    width: 100,
    flexGrow: 1,
    flexShrink: 1,
  },
  {
    id: "vatRate",
    label: "Taux de TVA",
    width: 100,
    key: "vatRate",
    renderCell: (accountingGroup: AccountingGroupI) => (
      <VatRate vatRate={accountingGroup.vatRate} />
    ),
  },
];

export const AccountingGroupsList: React.FC<Props> = ({ accountingGroups }) => {
  const navigate = useNavigate();
  const [, setProductsFilter] = useAtom(productsFiltersAtom);
  const goToProductsView = useCallback(
    (accountingGroup: AccountingGroupI) => {
      setProductsFilter({ query: "", accoutingGroupId: accountingGroup.id });
      navigate(generatePath(ROUTES[PRODUCTS]));
    },
    [setProductsFilter, navigate]
  );
  return (
    <DataTable
      columns={columns}
      data={accountingGroups}
      handleClickRow={goToProductsView}
    />
  );
};
