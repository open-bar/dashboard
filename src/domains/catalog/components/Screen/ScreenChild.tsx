import { Typography, styled } from "@mui/material";
import { ProductI, ScreenI } from "../../types";

interface Props {
  child: ScreenI | ProductI;
}

const ChildName = styled(Typography)({
  fontWeight: 500,
});

export const ScreenChild: React.FC<Props> = ({ child }) => {
  return (
    <>
      <ChildName>{child.name}</ChildName>
    </>
  );
};
