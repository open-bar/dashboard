import { Box, Container, IconButton, styled } from "@mui/material";
import { ProductI, ProductId, ScreenI, ScreenId } from "../../types";
import { MainTitle } from "@/components/MainTitle";
import { ArrowBack } from "@mui/icons-material";
import { useCallback, useMemo, useState } from "react";
import {
  DndContext,
  DragEndEvent,
  DragStartEvent,
  MouseSensor,
  TouchSensor,
  UniqueIdentifier,
  closestCenter,
  useSensor,
  useSensors,
} from "@dnd-kit/core";
import { SortableContext, rectSortingStrategy } from "@dnd-kit/sortable";
import { SortableScreen } from "./SortableScreen";
import { useAtom } from "jotai";
import { moveChildAfter } from "../../atoms";
import { isChildScreen } from "../../utils";
import { AddScreenChildMenu } from "./AddScreenChildMenu";

interface Props {
  screen: ScreenI;
  parents: ScreenI[];
  goBack: () => void;
}

const ChildrenContainer = styled(Box)({
  display: "flex",
  alignItems: "flex-start",
  justifyContent: "stretch",
  gap: "16px",
  flexWrap: "wrap",
});

export const ScreenView: React.FC<Props> = ({ screen, parents, goBack }) => {
  const [selectedChildId, setSelectedChildId] = useState<ScreenId | null>(null);
  const resetChildSelection = useCallback(() => setSelectedChildId(null), []);
  const setSelectedChild = useCallback(
    (child: ScreenI | null) => setSelectedChildId(child ? child.id : null),
    []
  );
  const selectedChild = useMemo(
    () =>
      screen.children
        .filter(isChildScreen)
        .find((child) => child.id === selectedChildId),
    [screen, selectedChildId]
  );

  const parentIds = useMemo(
    () => [...parents.map((parent) => parent.id), screen.id],
    [parents, screen.id]
  );

  const sensors = useSensors(
    useSensor(MouseSensor, { activationConstraint: { distance: 8 } }),
    useSensor(TouchSensor, { activationConstraint: { distance: 8 } })
  );
  const [activeId, setActiveId] = useState<UniqueIdentifier | null>(null);

  const [, moveChild] = useAtom(moveChildAfter);

  const handleDragStart = useCallback((event: DragStartEvent) => {
    setActiveId(event.active.id);
  }, []);

  const handleDragEnd = useCallback(
    (event: DragEndEvent) => {
      const { active, over } = event;

      if (over && active.id !== over.id) {
        moveChild(
          active.id as ScreenId | ProductId,
          over.id as ScreenId | ProductId,
          parentIds
        );
      }

      setActiveId(null);
    },
    [parentIds, moveChild]
  );

  const handleDragCancel = useCallback(() => {
    setActiveId(null);
  }, []);

  const renderScreenChild = useCallback(
    (child: ScreenI | ProductI, index: number) => (
      <SortableScreen
        key={child.id}
        index={index}
        child={child}
        activeId={activeId}
        setSelectedChild={setSelectedChild}
      />
    ),
    [setSelectedChild, activeId]
  );

  if (selectedChild) {
    return (
      <ScreenView
        screen={selectedChild}
        parents={[...parents, screen]}
        goBack={resetChildSelection}
      />
    );
  }
  return (
    <Container>
      <MainTitle
        title={`${parents.map((parent) => parent.name).join(" > ")}${
          parents.length > 0 ? " > " : ""
        }${screen.name}`}
        beforeTitle={
          <IconButton onClick={goBack}>
            <ArrowBack />
          </IconButton>
        }
      >
        <AddScreenChildMenu screenId={screen.id} />
      </MainTitle>

      <DndContext
        sensors={sensors}
        collisionDetection={closestCenter}
        onDragStart={handleDragStart}
        onDragEnd={handleDragEnd}
        onDragCancel={handleDragCancel}
      >
        <SortableContext items={screen.children} strategy={rectSortingStrategy}>
          <ChildrenContainer>
            {screen.children.map(renderScreenChild)}
          </ChildrenContainer>
        </SortableContext>
      </DndContext>
    </Container>
  );
};
