import { FC, useCallback } from "react";
import { ScreenI, ScreenId } from "../../types";
import { useScreen } from "../../hooks";
import { Alert, Container } from "@mui/material";
import { genericErrorMessage } from "@/utils";
import { Loader } from "@/components/Loader";
import { ScreenView } from "./ScreenView";
import { generatePath, useNavigate } from "react-router-dom";
import { ROUTES, SCREENS } from "@/routes";

interface Props {
  screenId: ScreenId;
}

const NO_PARENTS: ScreenI[] = [];

export const Screen: FC<Props> = ({ screenId }) => {
  const { result: screen, error } = useScreen(screenId);
  const navigate = useNavigate();
  const goBack = useCallback(() => {
    navigate(generatePath(ROUTES[SCREENS]));
  }, [navigate]);
  if (error) {
    return (
      <Container>
        <Alert severity="error">{genericErrorMessage[error.type]}</Alert>
      </Container>
    );
  }
  if (!screen) {
    return (
      <Container>
        <Loader />
      </Container>
    );
  }
  return <ScreenView screen={screen} parents={NO_PARENTS} goBack={goBack} />;
};
