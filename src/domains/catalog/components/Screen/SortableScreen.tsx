import React from "react";
import { UniqueIdentifier } from "@dnd-kit/core";
import { useSortable } from "@dnd-kit/sortable";
import { CSS } from "@dnd-kit/utilities";

import { ProductI, ScreenI } from "../../types";
import { ScreenChild } from "./ScreenChild";
import { Card, styled } from "@mui/material";
import invert from "invert-color";
import { isChildScreen } from "../../utils";

interface Props {
  child: ScreenI | ProductI;
  index: number;
  activeId: UniqueIdentifier | null;
  setSelectedChild: (screen: ScreenI | null) => void;
}

interface ContainerProps {
  color: string | undefined;
}

const ChildContainer = styled(Card, {
  shouldForwardProp: (prop) => prop !== "color",
})<ContainerProps>(({ color }) => ({
  minHeight: "160px",
  flex: "1 0 160px",
  maxWidth: "250px",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  backgroundColor: color,
  color: color ? invert(color, true) : "#000000",
  textTransform: "uppercase",
  cursor: "pointer",
  touchAction: "manipulation",
}));

export const SortableScreen: React.FC<Props> = ({
  child,
  setSelectedChild,
  ...props
}) => {
  const { attributes, listeners, setNodeRef, transform, transition } =
    useSortable({ id: child.id });

  const style = {
    transform: CSS.Transform.toString(transform),
    transition,
  };

  const onClick = () => {
    if (isChildScreen(child)) {
      setSelectedChild(child);
    }
  };

  return (
    <ChildContainer
      ref={setNodeRef}
      color={isChildScreen(child) ? child.color ?? undefined : undefined}
      style={style}
      {...props}
      {...attributes}
      {...listeners}
      onClick={onClick}
    >
      <ScreenChild child={child} />
    </ChildContainer>
  );
};
