import { Add, KeyboardArrowDown } from "@mui/icons-material";
import { Button, Menu, MenuItem } from "@mui/material";
import React, { MouseEvent, useCallback, useState } from "react";
import { AddProductToScreenModal } from "./AddProductToScreenModal";
import { ScreenId } from "../../types";

interface Props {
  screenId: ScreenId;
}

export const AddScreenChildMenu: React.FC<Props> = ({ screenId }) => {
  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);
  const [productModalOpen, setProductModalOpen] = useState<boolean>(false);
  const openAddMenu = useCallback(
    (event: MouseEvent<HTMLButtonElement>) => setAnchorEl(event.currentTarget),
    []
  );
  const handleClose = useCallback(() => setAnchorEl(null), []);
  const openProductModal = useCallback(() => setProductModalOpen(true), []);
  const closeProductModal = useCallback(() => {
    setProductModalOpen(false);
    setAnchorEl(null);
  }, []);
  return (
    <>
      <Button
        variant="outlined"
        endIcon={<KeyboardArrowDown />}
        startIcon={<Add />}
        onClick={openAddMenu}
      >
        Ajouter
      </Button>
      <Menu anchorEl={anchorEl} open={anchorEl !== null} onClose={handleClose}>
        <MenuItem onClick={openProductModal} disableRipple>
          Product
        </MenuItem>
        <MenuItem onClick={handleClose} disableRipple>
          Sous-écran
        </MenuItem>
      </Menu>
      <AddProductToScreenModal
        open={productModalOpen}
        close={closeProductModal}
        screenId={screenId}
      />
    </>
  );
};
