import { Loader } from "@/components/Loader";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  styled,
} from "@mui/material";
import React, { FormEvent, useCallback, useState } from "react";
import { SelectProduct } from "../SelectProduct";
import { ProductI, ScreenId } from "../../types";
import { useAtom } from "jotai";
import { addChildToScreenAtom } from "../../atoms";

interface Props {
  open: boolean;
  close: () => void;
  screenId: ScreenId;
}

const CenteredDialogContent = styled(DialogContent)({
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
});

export const AddProductToScreenModal: React.FC<Props> = ({
  open,
  close,
  screenId,
}) => {
  const [, addChildToScreen] = useAtom(addChildToScreenAtom);
  const [selectedProduct, setSelectedProduct] = useState<ProductI | null>(null);
  const loading = false;
  const onSubmit = useCallback(
    (event: FormEvent<HTMLFormElement>) => {
      event.preventDefault();
      if (selectedProduct) {
        addChildToScreen(selectedProduct, screenId);
        close();
      }
    },
    [selectedProduct, screenId, addChildToScreen, close]
  );
  return (
    <Dialog
      open={open}
      maxWidth="sm"
      fullWidth
      onClose={close}
      PaperProps={{
        component: "form",
        onSubmit: onSubmit,
      }}
    >
      <DialogTitle>Ajouter un produit à l'écran</DialogTitle>
      {loading ? (
        <CenteredDialogContent>
          <Loader />
        </CenteredDialogContent>
      ) : (
        <DialogContent>
          <SelectProduct
            value={selectedProduct}
            onChange={setSelectedProduct}
            variant="standard"
          />
        </DialogContent>
      )}
      <DialogActions>
        <Button onClick={close}>Annuler</Button>
        <Button type="submit" variant="contained">
          Créer
        </Button>
      </DialogActions>
    </Dialog>
  );
};
