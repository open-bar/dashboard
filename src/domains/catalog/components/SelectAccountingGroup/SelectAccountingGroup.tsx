import React, { HTMLAttributes, useCallback } from "react";
import { AccountingGroupI } from "../../types";
import {
  Autocomplete,
  AutocompleteValue,
  Box,
  SxProps,
  TextField,
  TextFieldVariants,
} from "@mui/material";
import { useAccountingGroups } from "../../hooks";
import { genericErrorMessage } from "@/utils/errors";
import { renderVatRate } from "../../utils";
import { isDefined } from "@/utils/isDefined";

interface Props {
  onChange: (accountingGroup: AccountingGroupI | null) => void;
  value: AccountingGroupI | null;
  required?: boolean;
  variant?: TextFieldVariants;
  sx?: SxProps;
  margin?: "normal" | "dense" | "none" | undefined;
}

const getOptionLabel = (accountingGroup: AccountingGroupI) =>
  `${accountingGroup.name} (${renderVatRate(accountingGroup.vatRate)})`;

const areAccountingGroupsEqual = (
  accountingGroup1: AccountingGroupI,
  accountingGroup2: AccountingGroupI
) => accountingGroup1.id === accountingGroup2.id;

const listProps: HTMLAttributes<HTMLUListElement> = {
  "data-testid": "select-accounting-group-options",
} as HTMLAttributes<HTMLUListElement>;

export const SelectAccountingGroup: React.FC<Props> = ({
  value,
  onChange,
  required,
  variant,
  sx,
  margin = "none",
}) => {
  const { result: accountingGroups, loading, error } = useAccountingGroups();
  const handleChange = useCallback(
    (
      _event: React.SyntheticEvent,
      newValue: AutocompleteValue<AccountingGroupI | null, false, false, false>
    ) => {
      onChange(newValue);
    },
    [onChange]
  );

  return (
    <Autocomplete
      data-testid="select-accounting-group"
      value={value}
      disablePortal
      onChange={handleChange}
      renderInput={(params) => (
        <TextField
          {...params}
          label="Groupe comptable"
          error={!!error}
          variant={variant}
          margin={margin}
          helperText={error ? genericErrorMessage[error.type] : null}
          required={required}
          inputProps={{
            ...params.inputProps,
            required: required && isDefined(value),
          }}
        />
      )}
      loading={loading}
      loadingText="Chargement des groupes comptables..."
      options={accountingGroups ?? []}
      isOptionEqualToValue={areAccountingGroupsEqual}
      getOptionLabel={getOptionLabel}
      renderOption={(props, option) => (
        <Box component="li" {...props} key={option.id}>
          {getOptionLabel(option)}
        </Box>
      )}
      noOptionsText="Aucun groupe comptable correspondant"
      ListboxProps={listProps}
      sx={sx}
    />
  );
};
