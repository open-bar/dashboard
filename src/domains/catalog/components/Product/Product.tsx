import React, { useCallback } from "react";
import { ProductI, ProductId, ProductInCreationI } from "../../types";
import { Alert, Button, Container, IconButton } from "@mui/material";
import { Loader } from "@/components/Loader";
import { useDeleteProduct, useProduct, useUpdateProduct } from "../../hooks";
import { EditProductForm } from "../EditProductForm";
import { MainTitle } from "@/components/MainTitle";
import { genericErrorMessage } from "@/utils/errors";
import { ArrowBack } from "@mui/icons-material";
import { generatePath, useNavigate } from "react-router-dom";
import { PRODUCTS, ROUTES } from "@/routes";
import { ConfirmModal } from "@/domains/common/utils/components/ConfirmModal";
import { useBooleanState } from "@/domains/common/utils/hooks";

interface Props {
  productId: ProductId;
}

export const Product: React.FC<Props> = ({ productId }) => {
  const { result: product, error } = useProduct(productId);
  const updateProduct = useUpdateProduct(productId);
  const deleteProduct = useDeleteProduct(productId);
  const {
    value: confirmModalVisible,
    setTrue: openConfirmModal,
    setFalse: closeConfirmModal,
  } = useBooleanState();
  const navigate = useNavigate();
  const onProductChange = useCallback(
    ({ accountingGroup, ...updatePayload }: Partial<ProductInCreationI>) => {
      const payload: Partial<Omit<ProductI, "id">> = {
        ...updatePayload,
      };
      if (accountingGroup) {
        payload.accountingGroup = accountingGroup;
      }
      updateProduct(payload);
    },
    [updateProduct]
  );
  const onProductDelete = useCallback(async () => {
    const result = await deleteProduct();
    navigate(generatePath(ROUTES[PRODUCTS]));
    return result;
  }, [deleteProduct, navigate]);
  if (error) {
    return (
      <Container>
        <Alert severity="error">{genericErrorMessage[error.type]}</Alert>
      </Container>
    );
  }
  if (!product) {
    return (
      <Container>
        <Loader />
      </Container>
    );
  }
  return (
    <Container>
      <MainTitle
        title={product.name}
        beforeTitle={
          <IconButton onClick={() => navigate(generatePath(ROUTES[PRODUCTS]))}>
            <ArrowBack />
          </IconButton>
        }
      />
      <EditProductForm product={product} onChange={onProductChange} />
      <Button color="error" variant="contained" onClick={openConfirmModal}>
        Archiver le produit
      </Button>
      <ConfirmModal
        open={confirmModalVisible}
        close={closeConfirmModal}
        onConfirm={onProductDelete}
        title={`Archiver ${product.name} ?`}
      >
        Êtes vous sûr·e de vouloir archiver le produit {product.name} ?
      </ConfirmModal>
    </Container>
  );
};
