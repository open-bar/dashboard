import React, {
  ChangeEvent,
  KeyboardEvent,
  useCallback,
  useState,
} from "react";
import { AmountI } from "../types/Product";
import { TextField } from "@mui/material";
import { createAmount, getAmountValue, renderAmount } from "../utils";

interface Props {
  value: AmountI | null;
  onChange: (value: AmountI) => void;
  label: string;
  name: string;
}

const onKeyDown = (e: KeyboardEvent<HTMLInputElement>) => {
  if (e.key === "Enter") {
    e.currentTarget.blur();
  }
};

export const AmountField: React.FC<Props> = ({
  value,
  onChange,
  label,
  name,
}) => {
  const [focus, setFocus] = useState<boolean>(false);

  const handleChange = useCallback(
    (event: ChangeEvent<HTMLInputElement>) => {
      const value = parseFloat(event.currentTarget.value);
      if (!isNaN(value)) {
        onChange(createAmount(value));
      }
    },
    [onChange]
  );

  return (
    <TextField
      value={value ? (focus ? getAmountValue(value) : renderAmount(value)) : ""}
      onChange={handleChange}
      onFocus={() => setFocus(true)}
      onBlur={() => setFocus(false)}
      focused={focus}
      margin="none"
      required
      fullWidth
      name={name}
      label={label}
      inputProps={{ "aria-label": label, onKeyDown }}
      type={focus ? "number" : "text"}
      autoComplete="off"
    />
  );
};
