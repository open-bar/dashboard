import React from "react";
import { VatRateI } from "../types/AccountingGroup";
import { SxProps, Typography } from "@mui/material";
import { renderVatRate } from "../utils";
import { Variant } from "@mui/material/styles/createTypography";

interface Props {
  vatRate: VatRateI;
  sx?: SxProps;
  variant?: Variant;
}

const variantMapping: Partial<Record<Variant, string>> = {
  body2: "span",
};

export const VatRate: React.FC<Props> = ({ vatRate, sx, variant }) => {
  return (
    <Typography sx={sx} variant={variant} variantMapping={variantMapping}>
      {renderVatRate(vatRate)}
    </Typography>
  );
};
