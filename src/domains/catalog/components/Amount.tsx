import React from "react";
import { SxProps, Typography } from "@mui/material";
import { renderAmount } from "../utils";
import { AmountI } from "../types/Product";
import { isDefined } from "@/utils/isDefined";
import { Variant } from "@mui/material/styles/createTypography";

interface Props {
  value: AmountI | null;
  sx?: SxProps;
  variant?: Variant;
}

const variantMapping: Partial<Record<Variant, string>> = {
  body2: "span",
};

export const Amount: React.FC<Props> = ({ value, sx, variant }) => {
  return (
    <Typography sx={sx} variant={variant} variantMapping={variantMapping}>
      {isDefined(value) ? renderAmount(value) : "-"}
    </Typography>
  );
};
