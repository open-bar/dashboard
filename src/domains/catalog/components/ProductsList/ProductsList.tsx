import React, { useCallback } from "react";
import { Typography } from "@mui/material";
import { ProductI } from "../../types/Product";
import { DataTable, DataTableColumn } from "@/components/DataTable";
import { Amount } from "../Amount";
import { VatRate } from "../VatRate";
import { generatePath, useNavigate } from "react-router-dom";
import { EDIT_PRODUCT, ROUTES } from "@/routes";
import { ProductsFilters } from "./ProductsFilters";

interface Props {
  products: ProductI[];
}

const getProductAccountingGroupName = (product: ProductI) =>
  product.accountingGroup.name;

const getProductVatRate = (product: ProductI) =>
  product.accountingGroup.vatRate;

const columns: DataTableColumn<ProductI>[] = [
  {
    id: "name",
    label: "Nom",
    key: "name",
    width: 100,
    flexGrow: 1,
    flexShrink: 1,
  },
  {
    id: "accountingGroupName",
    label: "Catégorie",
    width: 150,
    getValue: getProductAccountingGroupName,
  },
  {
    id: "sellPrice",
    label: "Prix de vente",
    key: "sellPrice",
    width: 150,
    renderCell: (product: ProductI) =>
      product.freePrice ? (
        <Typography>Prix libre</Typography>
      ) : (
        <Amount value={product.sellPrice} sx={{ flex: "0 0 150px" }} />
      ),
  },
  {
    id: "purchasePrice",
    label: "Coût d'achat",
    key: "purchasePrice",
    width: 150,
    renderCell: (product: ProductI) => (
      <Amount value={product.purchasePrice} sx={{ flex: "0 0 150px" }} />
    ),
  },
  {
    id: "vatRate",
    label: "Taux de TVA",
    getValue: getProductVatRate,
    width: 100,
    renderCell: (product: ProductI) => (
      <VatRate vatRate={product.accountingGroup.vatRate} />
    ),
  },
];

export const ProductsList: React.FC<Props> = ({ products }) => {
  const navigate = useNavigate();
  const navigateToProduct = useCallback(
    (product: ProductI) => {
      navigate(generatePath(ROUTES[EDIT_PRODUCT], { productId: product.id }));
    },
    [navigate]
  );
  return (
    <>
      <ProductsFilters />
      <DataTable
        data={products}
        columns={columns}
        handleClickRow={navigateToProduct}
        noItemsMessage="Aucun product correspondant"
      />
    </>
  );
};
