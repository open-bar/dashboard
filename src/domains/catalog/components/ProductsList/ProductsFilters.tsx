import { FC } from "react";
import {
  useProductsAccountingGroupFilter,
  useProductsFilterQuery,
} from "../../hooks";
import { TextField, styled } from "@mui/material";
import Grid from "@mui/material/Unstable_Grid2"; // Grid version 2
import { SelectAccountingGroup } from "../SelectAccountingGroup";

const Container = styled(Grid)({
  margin: "-40px 0 16px",
});

export const ProductsFilters: FC = () => {
  const [query, setQuery] = useProductsFilterQuery();
  const [accoutingGroup, setAccountingGroup] =
    useProductsAccountingGroupFilter();
  return (
    <Container container spacing={2}>
      <Grid xs={12} sm={6} md={4}>
        <TextField
          value={query}
          onChange={setQuery}
          margin="none"
          variant="standard"
          required
          name="products-query-filter"
          label="Filtrer les produits"
          inputProps={{ "aria-label": "Filtrer les produicts" }}
          type="text"
          autoComplete="off"
          fullWidth
          sx={{ flex: "1 1 auto" }}
        />
      </Grid>
      <Grid xs={12} sm={6} md={3}>
        <SelectAccountingGroup
          onChange={setAccountingGroup}
          value={accoutingGroup}
          variant="standard"
          sx={{ flex: "1 1 auto" }}
        />
      </Grid>
    </Container>
  );
};
