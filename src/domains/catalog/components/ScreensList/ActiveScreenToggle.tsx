import React from "react";
import { ScreenI } from "../../types/Screen";
import { Switch } from "@mui/material";
import { useUpdateScreen } from "../../hooks/useUpdateScreen";

interface Props {
  screen: ScreenI;
}

const stopPropagation = (e: React.SyntheticEvent) => {
  e.stopPropagation();
};

export const ActiveScreenToggle: React.FC<Props> = ({ screen }) => {
  const updateScreen = useUpdateScreen(screen.id);
  return (
    <Switch
      onMouseDown={stopPropagation}
      onClick={stopPropagation}
      checked={screen.active === true}
      onChange={(_e, value) => {
        updateScreen({ active: value });
      }}
    />
  );
};
