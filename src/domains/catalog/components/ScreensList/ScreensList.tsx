import React, { useCallback } from "react";
import { ScreenI } from "../../types/Screen";
import { ActiveScreenToggle } from "./ActiveScreenToggle";
import { DataTable, DataTableColumn } from "@/components/DataTable";
import { useAtom } from "jotai";
import { moveSceenAfterAtom } from "../../atoms";
import { generatePath, useNavigate } from "react-router-dom";
import { EDIT_SCREEN, ROUTES } from "@/routes";

interface Props {
  screens: ScreenI[];
}

const columns: DataTableColumn<ScreenI>[] = [
  {
    id: "name",
    label: "Nom",
    key: "name",
    width: 100,
    flexGrow: 1,
    flexShrink: 1,
  },
  {
    id: "active",
    label: "Actif",
    width: 58,
    key: "active",
    renderCell: (screen: ScreenI) => <ActiveScreenToggle screen={screen} />,
  },
];

export const ScreensList: React.FC<Props> = ({ screens }) => {
  const navigate = useNavigate();
  const [, moveScreen] = useAtom(moveSceenAfterAtom);
  const moveScreenAfter = useCallback(
    (item: ScreenI, afterItem: ScreenI) => {
      moveScreen(item.id, afterItem.id);
    },
    [moveScreen]
  );
  const navigateToScreen = useCallback(
    (screen: ScreenI) => {
      navigate(generatePath(ROUTES[EDIT_SCREEN], { screenId: screen.id }));
    },
    [navigate]
  );
  return (
    <>
      <DataTable
        columns={columns}
        data={screens}
        sortable
        moveItemAfter={moveScreenAfter}
        handleClickRow={navigateToScreen}
      />
    </>
  );
};
