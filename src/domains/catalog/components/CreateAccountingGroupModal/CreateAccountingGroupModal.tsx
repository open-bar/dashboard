import {
  Alert,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
} from "@mui/material";
import React, { useCallback, useState } from "react";
import { VatRateI } from "../../types/AccountingGroup";
import { createVatRate, getVatRateValue } from "../../utils";
import { useCreateAccountingGroup } from "../../hooks";
import { Loader } from "@/components/Loader";

interface Props {
  open: boolean;
  close: () => void;
}

export const CreateAccountingGroupModal: React.FC<Props> = ({
  open,
  close,
}) => {
  const [name, setName] = useState<string>("");
  const [vatRate, setVatRate] = useState<VatRateI>(createVatRate(20));

  const { createAccountingGroup, loading, error } = useCreateAccountingGroup();

  const onSubmit = useCallback(
    (event: React.FormEvent<HTMLFormElement>) => {
      event.preventDefault();
      createAccountingGroup({ name, vatRate });
      setName("");
      close();
    },
    [name, vatRate, close, createAccountingGroup]
  );
  return (
    <Dialog
      open={open}
      onClose={close}
      PaperProps={{
        component: "form",
        onSubmit: onSubmit,
      }}
    >
      <DialogTitle>Créer un groupe comptable</DialogTitle>
      {loading ? (
        <DialogContent>
          <Loader />
        </DialogContent>
      ) : (
        <DialogContent>
          <TextField
            autoFocus
            value={name}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              setName(event.currentTarget.value);
            }}
            required
            margin="dense"
            name="name"
            label="Nom du groupe comptable"
            inputProps={{ "aria-label": "Nom du groupe comptable" }}
            type="text"
            fullWidth
            variant="standard"
          />
          <TextField
            value={100 * getVatRateValue(vatRate)}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              const value = parseFloat(event.currentTarget.value);
              if (!isNaN(value)) {
                setVatRate(createVatRate(value));
              }
            }}
            required
            margin="dense"
            name="vatRate"
            label="Taux de TVA"
            type="number"
            InputProps={{
              inputProps: {
                min: 0,
                max: 100,
                step: 0.1,
                "aria-label": "Taux de TVA",
              },
            }}
            fullWidth
            variant="standard"
          />
          {error && <Alert severity="error">{error.type}</Alert>}
        </DialogContent>
      )}
      <DialogActions>
        <Button onClick={close}>Annuler</Button>
        <Button type="submit">Créer</Button>
      </DialogActions>
    </Dialog>
  );
};
