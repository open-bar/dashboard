import { Opaque } from "@/utils/Opaque";
import * as t from "io-ts";

export type AccountingGroupId = Opaque<string, { readonly T: unique symbol }>;
export type VatRateI = Opaque<number, { readonly T: unique symbol }>;

export const AccountingGroupIO = t.type({
  id: t.string,
  name: t.string,
  vatRate: t.number,
});

export type AccountingGroupI = t.TypeOf<typeof AccountingGroupIO> & {
  id: AccountingGroupId;
  vatRate: VatRateI;
};

export type CreateAccountingGroupPayload = {
  name: string;
  vatRate: VatRateI;
};
