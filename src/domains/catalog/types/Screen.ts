import { Opaque } from "@/utils/Opaque";
import { PaginatedResultsI, PaginationInfoIO } from "@/utils/pagination";
import * as t from "io-ts";
import { ProductI, ProductIO } from "./Product";
import { optional } from "@/utils";

export type ScreenId = Opaque<string, { readonly T: unique symbol }>;

type ScreenPartialI = {
  id: string;
  name: string;
  order: string;
  active: boolean;
  children: (ScreenPartialI | t.TypeOf<typeof ProductIO>)[];
  image?: string | null;
  color?: string | null;
};

const ScreenIO: t.Type<ScreenPartialI> = t.recursion("ScreenIO", () =>
  t.intersection([
    t.type({
      id: t.string,
      name: t.string,
      order: t.string,
      active: t.boolean,
      children: t.array(t.union([ScreenIO, ProductIO])),
    }),
    t.partial({
      image: optional(t.string),
      color: optional(t.string),
    }),
  ])
);

export const PaginatedScreensIO = t.intersection([
  PaginationInfoIO,
  t.type({
    results: t.array(ScreenIO),
  }),
]);

export type ScreenI = {
  id: ScreenId;
  name: string;
  order: string;
  active: boolean;
  children: (ScreenI | ProductI)[];
  image?: string | null;
  color?: string | null;
};

export type PaginatedScreensI = PaginatedResultsI<ScreenI>;
