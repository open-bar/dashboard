import { Opaque } from "@/utils/Opaque";
import { optional } from "@/utils/optional";
import * as t from "io-ts";
import {
  AccountingGroupI,
  AccountingGroupIO,
  AccountingGroupId,
} from "./AccountingGroup";

export type ProductId = Opaque<string, { readonly T: unique symbol }>;
export type AmountI = Opaque<number, { readonly T: unique symbol }>;

export const ProductIO = t.intersection([
  t.type({
    id: t.string,
    name: t.string,
    accountingGroup: AccountingGroupIO,
  }),
  t.partial({
    shortName: optional(t.string),
    purchasePrice: optional(t.number),
    sellPrice: optional(t.number),
    image: optional(t.string),
    freePrice: optional(t.boolean),
  }),
]);

export type ProductI = t.TypeOf<typeof ProductIO> & {
  id: ProductId;
  purchasePrice: AmountI | null;
  sellPrice: AmountI | null;
  accountingGroup: AccountingGroupI;
  children?: never;
};

export type ProductInCreationI = Omit<
  Omit<ProductI, "id">,
  "accountingGroup"
> & {
  accountingGroup: AccountingGroupI | null;
};

export type CreateProductPayload = {
  accountingGroupId: AccountingGroupId;
  name: string;
  shortName: string | undefined | null;
  purchasePrice: AmountI | null;
  sellPrice: AmountI | null;
  image: string | null | undefined;
  freePrice: boolean | null | undefined;
};
