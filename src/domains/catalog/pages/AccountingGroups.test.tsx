import { afterEach, describe, test } from "vitest";
import { asAVisitor } from "@/tests/Visitor";
import { asARegisteredUser } from "@/tests";
import { createVatRate } from "../utils";

afterEach(() => {});

describe("Accounting Groups", () => {
  test("Visitor cannot access to accounting groups", async () => {
    await asAVisitor().visisiting("/catalog/groups").isOnTheLoginPage();
  });
  test("Registered user can view accounting groups", async () => {
    await asARegisteredUser()
      .visisiting("/catalog/groups/")
      .isOnTheAccountingGroupsPage()
      .then((user) =>
        user.canViewAccountingGroup({
          name: "Bières",
          vatRate: createVatRate(20),
        })
      )
      .then((user) => user.clickOnCreateAccoutingGroup());
  });
});
