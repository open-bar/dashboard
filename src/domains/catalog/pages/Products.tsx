import { MainTitle } from "@/components/MainTitle";
import { CREATE_PRODUCT, ROUTES } from "@/routes";
import { Add } from "@mui/icons-material";
import { Alert, Button, Container } from "@mui/material";
import React from "react";
import { generatePath, useNavigate } from "react-router-dom";
import { useProducts } from "../hooks";
import { Loader } from "@/components/Loader";
import { genericErrorMessage } from "@/utils/errors";
import { ProductsList } from "../components/ProductsList";

const Products: React.FC = () => {
  const { result: products, loading, error } = useProducts();
  const navigate = useNavigate();
  return (
    <Container>
      <MainTitle title="Produits">
        <Button
          variant="outlined"
          startIcon={<Add />}
          onClick={() => navigate(generatePath(ROUTES[CREATE_PRODUCT]))}
        >
          Ajouter
        </Button>
      </MainTitle>
      {loading && <Loader />}
      {error && (
        <Alert severity="error">{genericErrorMessage[error.type]}</Alert>
      )}
      {products && <ProductsList products={products} />}
    </Container>
  );
};

export default Products;
