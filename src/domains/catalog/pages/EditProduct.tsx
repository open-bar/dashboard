import React, { useEffect } from "react";
import { generatePath, useNavigate, useParams } from "react-router-dom";
import { ProductId } from "../types";
import { PRODUCTS, ROUTES } from "@/routes";
import { Product } from "../components/Product";

const EditProduct: React.FC = () => {
  const { productId } = useParams<{ productId: ProductId }>();
  const navigate = useNavigate();

  useEffect(() => {
    if (!productId) {
      navigate(generatePath(ROUTES[PRODUCTS]), { replace: true });
    }
  }, [productId, navigate]);

  if (!productId) {
    return null;
  }
  return <Product productId={productId} />;
};

export default EditProduct;
