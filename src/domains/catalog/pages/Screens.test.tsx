import { afterEach, describe, test } from "vitest";
import { asAVisitor } from "@/tests/Visitor";
import { asARegisteredUser } from "@/tests";

afterEach(() => {});

describe("Screens", () => {
  test("Visitor cannot access to screens page", async () => {
    await asAVisitor().visisiting("/catalog/screens").isOnTheLoginPage();
  });
  test("Registered user can view screens", async () => {
    await asARegisteredUser()
      .visisiting("/catalog/screens")
      .currentPageHasTitle("Écrans");
  });
});
