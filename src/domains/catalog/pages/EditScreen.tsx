import React, { useEffect } from "react";
import { generatePath, useNavigate, useParams } from "react-router-dom";
import { ScreenId } from "../types";
import { ROUTES, SCREENS } from "@/routes";
import { Screen } from "../components/Screen";

const EditScreen: React.FC = () => {
  const { screenId } = useParams<{ screenId: ScreenId }>();
  const navigate = useNavigate();

  useEffect(() => {
    if (!screenId) {
      navigate(generatePath(ROUTES[SCREENS]), { replace: true });
    }
  }, [screenId, navigate]);

  if (!screenId) {
    return null;
  }
  return <Screen screenId={screenId} />;
};

export default EditScreen;
