import { describe, test } from "vitest";
import { asAVisitor } from "@/tests/Visitor";
import { asARegisteredUser } from "@/tests";

describe("Edit Product", () => {
  test("Visitor cannot access to the create product page", async () => {
    await asAVisitor()
      .visisiting("/catalog/products/ebdc0243-9398-4419-8f18-3e4b6e1ded16")
      .isOnTheLoginPage();
  });
  test("Registered user can create a product", async () => {
    await asARegisteredUser()
      .visisiting("/catalog/products/ebdc0243-9398-4419-8f18-3e4b6e1ded16")
      .currentPageHasTitle("Pinte de blonde");
  });
});
