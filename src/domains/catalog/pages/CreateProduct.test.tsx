import { describe, test } from "vitest";
import { asAVisitor } from "@/tests/Visitor";
import { asARegisteredUser } from "@/tests";
import { createAmount } from "../utils";
import { mockedAccountingGroups } from "../providers/CatalogProvider.inMemory";

describe("Create Product", () => {
  test("Visitor cannot access to the create product page", async () => {
    await asAVisitor().visisiting("/catalog/create-product").isOnTheLoginPage();
  });
  test("Registered user can create a product", async () => {
    await asARegisteredUser()
      .visisiting("/catalog/create-product")
      .currentPageHasTitle("Création d'un produit")
      .then((user) =>
        user.createProduct({
          name: "Bières",
          purchasePrice: createAmount(3.5),
          sellPrice: createAmount(7),
          accountingGroup: mockedAccountingGroups[0],
        })
      );
  });
});
