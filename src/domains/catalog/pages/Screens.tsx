import { Container, Typography } from "@mui/material";
import React from "react";
import { useScreens } from "../hooks";
import { Loader } from "@/components/Loader";
import { ScreensList } from "../components/ScreensList";

const Screens: React.FC = () => {
  const { result: screens, loading } = useScreens();
  return (
    <Container>
      <Typography variant="h2" role="heading" aria-level={1}>
        Écrans
      </Typography>
      {loading && <Loader />}
      {screens && <ScreensList screens={screens} />}
    </Container>
  );
};

export default Screens;
