import { MainTitle } from "@/components/MainTitle";
import { PRODUCTS, ROUTES } from "@/routes";
import { ArrowBack } from "@mui/icons-material";
import { Alert, Button, Container, IconButton } from "@mui/material";
import React, { FormEvent, useCallback, useState } from "react";
import { generatePath, useNavigate } from "react-router-dom";
import { ProductInCreationI } from "../types/Product";
import { createAmount } from "../utils";
import { EditProductForm } from "../components/EditProductForm";
import { useCreateProduct } from "../hooks";
import { genericErrorMessage } from "@/utils/errors";
import { isResultSuccess } from "@/utils/Result";

const CreateProduct: React.FC = () => {
  const navigate = useNavigate();

  const [product, setProduct] = useState<ProductInCreationI>({
    name: "",
    shortName: "",
    accountingGroup: null,
    purchasePrice: createAmount(0),
    sellPrice: createAmount(0),
  });

  const { createProduct, error } = useCreateProduct();

  const onProductChange = useCallback(
    (payload: Partial<ProductInCreationI>) => {
      setProduct((currentProduct) => ({
        ...currentProduct,
        ...payload,
      }));
    },
    []
  );

  const onSubmit = useCallback(
    (event: FormEvent) => {
      event.preventDefault();
      createProduct(product).then((result) => {
        if (isResultSuccess(result)) {
          navigate(generatePath(ROUTES[PRODUCTS]));
        }
      });
    },
    [createProduct, product, navigate]
  );

  return (
    <Container>
      <MainTitle
        beforeTitle={
          <IconButton onClick={() => navigate(generatePath(ROUTES[PRODUCTS]))}>
            <ArrowBack />
          </IconButton>
        }
        title="Création d'un produit"
      ></MainTitle>
      <form onSubmit={onSubmit}>
        <EditProductForm product={product} onChange={onProductChange} />
        {error && (
          <Alert severity="error">{genericErrorMessage[error.type]}</Alert>
        )}
        <Button type="submit" variant="contained">
          Créer le produit
        </Button>
      </form>
    </Container>
  );
};

export default CreateProduct;
