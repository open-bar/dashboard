import { afterEach, describe, test } from "vitest";
import { asAVisitor } from "@/tests/Visitor";
import { asARegisteredUser } from "@/tests";

afterEach(() => {});

describe("Products", () => {
  test("Visitor cannot access to products page", async () => {
    await asAVisitor().visisiting("/catalog/products").isOnTheLoginPage();
  });
  test("Registered user can view products", async () => {
    await asARegisteredUser()
      .visisiting("/catalog/products/")
      .currentPageHasTitle("Produits");
  });
});
