import { Alert, Button, Container, Typography } from "@mui/material";
import React, { useState } from "react";
import { useAccountingGroups } from "../hooks";
import { Loader } from "@/components/Loader";
import { AccountingGroupsList } from "../components/AccountingGroupsList";
import { Add } from "@mui/icons-material";
import { MainTitle } from "@/components/MainTitle";
import { CreateAccountingGroupModal } from "../components/CreateAccountingGroupModal";
import { genericErrorMessage } from "@/utils/errors";

const AccountingGroups: React.FC = () => {
  const { result: accountingGroups, loading, error } = useAccountingGroups();
  const [createModalVisible, setCreateModalVisible] = useState<boolean>(false);
  return (
    <Container>
      <MainTitle title="Groupes comptables">
        <Button
          variant="outlined"
          startIcon={<Add />}
          onClick={() => setCreateModalVisible(true)}
        >
          Ajouter
        </Button>
      </MainTitle>
      <Typography variant="body2">
        Un groupe comptable permet de regrouper un ensemble de produits
        partageant le même taux de TVA. <br />
        Il permet aussi de réaliser des statistiques par groupe comptable. Il
        est donc intéressant de regrouper les produits dans des ensembles
        cohérents.
      </Typography>
      {loading && <Loader />}
      {error && (
        <Alert severity="error">{genericErrorMessage[error.type]}</Alert>
      )}
      {accountingGroups && (
        <AccountingGroupsList accountingGroups={accountingGroups} />
      )}
      <CreateAccountingGroupModal
        open={createModalVisible}
        close={() => setCreateModalVisible(false)}
      />
    </Container>
  );
};

export default AccountingGroups;
