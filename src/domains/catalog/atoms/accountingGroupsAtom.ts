import { atomWithImmer } from "jotai-immer";
import { atom } from "jotai";
import { AccountingGroupI, AccountingGroupId } from "../types/AccountingGroup";

export const accountingGroupsAtom = atomWithImmer<AccountingGroupI[] | null>(
  null
);
export const updateAccountingGroup = atom(
  null,
  (
    get,
    set,
    accoutingGroupId: AccountingGroupId,
    payload: Partial<Omit<AccountingGroupI, "id">>
  ) => {
    const accountingGroups = get(accountingGroupsAtom);
    if (accountingGroups) {
      set(
        accountingGroupsAtom,
        accountingGroups.map((accountingGroup) =>
          accountingGroup.id === accoutingGroupId
            ? { ...accountingGroup, ...payload }
            : accountingGroup
        ) ?? null
      );
      return (
        accountingGroups.find((accountingGroup) => accountingGroup.id === accoutingGroupId) ??
        null
      );
    }
    return null;
  }
);
