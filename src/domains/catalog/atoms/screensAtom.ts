import { atomWithImmer } from "jotai-immer";
import { ScreenI, ScreenId } from "../types/Screen";
import { atom } from "jotai";
import { ProductI, ProductId } from "../types";
import { isChildScreen } from "../utils";
import { isDefined } from "@/utils";
import { arrayMove } from "@dnd-kit/sortable";

export const screensAtom = atomWithImmer<ScreenI[] | null>(null);
export const updateScreenAtom = atom(
  null,
  (get, set, screenId: ScreenId, payload: Partial<Omit<ScreenI, "id">>) => {
    const screens = get(screensAtom);
    if (screens) {
      set(
        screensAtom,
        screens.map((screen) =>
          screen.id === screenId ? { ...screen, ...payload } : screen
        ) ?? null
      );
      return screens.find((screen) => screen.id === screenId) ?? null;
    }
    return null;
  }
);

export const moveSceenAfterAtom = atom(
  null,
  // in case moveAfterRef is null, screen is moved at the first position
  (get, set, movedScreenId: ScreenId, moveAfterRef: ScreenId) => {
    const screens = get(screensAtom);
    if (screens) {
      const movedScreenIndex = screens.findIndex(
        (screen) => screen.id === movedScreenId
      );
      const newIndex = screens.findIndex(
        (screen) => screen.id === moveAfterRef
      );
      if (movedScreenIndex >= 0 && newIndex >= 0) {
        set(screensAtom, arrayMove(screens, movedScreenIndex, newIndex));
      }
    }
    return null;
  }
);

const moveChild = (
  parent: ScreenI,
  movedChildId: ScreenId | ProductId,
  movedAfterChildId: ScreenId | ProductId,
  parentIds: ScreenId[]
): ScreenI => {
  const [parentId, ...otherParentIds] = parentIds;
  if (parent.id === parentId) {
    if (otherParentIds.length === 0) {
      const movedChildIndex = parent.children.findIndex(
        (child) => child.id === movedChildId
      );
      const movedAfterChildIndex = parent.children.findIndex(
        (child) => child.id === movedAfterChildId
      );
      if (isDefined(movedChildIndex) && isDefined(movedAfterChildIndex)) {
        const newChildren = arrayMove(
          parent.children,
          movedChildIndex,
          movedAfterChildIndex
        );
        return {
          ...parent,
          children: newChildren,
        };
      }
    }
    return {
      ...parent,
      children: parent.children.map((child) =>
        isChildScreen(child)
          ? moveChild(child, movedChildId, movedAfterChildId, otherParentIds)
          : child
      ),
    };
  }
  return parent;
};

export const moveChildAfter = atom(
  null,
  (
    get,
    set,
    movedChildId: ScreenId | ProductId,
    movedAfterChildId: ScreenId | ProductId,
    parents: ScreenId[]
  ) => {
    const screens = get(screensAtom);
    if (!screens) return;
    set(
      screensAtom,
      screens.map((screen) =>
        moveChild(screen, movedChildId, movedAfterChildId, parents)
      )
    );
  }
);

const addChildToScreen = (
  screen: ScreenI,
  child: ScreenI | ProductI,
  screenId: ScreenId
): ScreenI => {
  if (screen.id === screenId) {
    return {
      ...screen,
      children: [...screen.children, child],
    };
  }
  return {
    ...screen,
    children: screen.children.map((subScreen) =>
      isChildScreen(subScreen)
        ? addChildToScreen(subScreen, child, screenId)
        : subScreen
    ),
  };
};

export const addChildToScreenAtom = atom(
  null,
  (get, set, child: ScreenI | ProductI, screenId: ScreenId) => {
    const screens = get(screensAtom);
    if (!screens) return;
    set(
      screensAtom,
      screens.map((screen) => addChildToScreen(screen, child, screenId))
    );
  }
);
