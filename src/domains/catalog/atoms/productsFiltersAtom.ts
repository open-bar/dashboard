import { atomWithImmer } from "jotai-immer";
import { AccountingGroupId } from "../types";

type ProductsFilter = {
  query: string;
  accoutingGroupId: AccountingGroupId | null;
};

export const productsFiltersAtom = atomWithImmer<ProductsFilter>({
  query: "",
  accoutingGroupId: null,
});
