import { atomWithImmer } from "jotai-immer";
import { atom } from "jotai";
import { ProductI, ProductId } from "../types/Product";

export const productsAtom = atomWithImmer<ProductI[] | null>(null);
export const addProductAtom = atom(
  productsAtom,
  (get, set, product: ProductI) => {
    const products = get(productsAtom);
    set(productsAtom, [
      ...(products
        ? products.filter((product) => product.id !== product.id)
        : []),
      product,
    ]);
  }
);
export const updateProductAtom = atom(
  null,
  (get, set, productId: ProductId, payload: Partial<Omit<ProductI, "id">>) => {
    const products = get(productsAtom);
    if (products) {
      set(
        productsAtom,
        products.map((product) =>
          product.id === productId ? { ...product, ...payload } : product
        )
      );
      return products.find((product) => product.id === productId) ?? null;
    }
    return null;
  }
);

export const deleteProductAtom = atom(
  null,
  (get, set, productId: ProductId) => {
    const products = get(productsAtom);
    if (products) {
      set(
        productsAtom,
        products.filter((product) => product.id !== productId)
      );
      return products.find((product) => product.id === productId);
    }
    return null;
  }
);
