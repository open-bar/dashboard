import { styled } from "@mui/material";
import React from "react";
import { Header } from "./components/Header";

const MainContainer = styled("div")({
  height: "100vh",
  width: "100%",
  display: "flex",
  flexDirection: "column",
  alignItems: "stretch",
});

const FullPageLayout: React.FC<React.PropsWithChildren> = ({ children }) => {
  return (
    <MainContainer>
      <Header />
      {children}
    </MainContainer>
  );
};

export default FullPageLayout;
