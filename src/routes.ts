export const DASHBOARD = Symbol();

export const CATALOG = Symbol();

export const EDIT_SCREEN = Symbol();
export const SCREENS = Symbol();

export const CREATE_PRODUCT = Symbol();
export const PRODUCTS = Symbol();
export const EDIT_PRODUCT = Symbol();

export const ACCOUNTING_GROUPS = Symbol();

export const LOGIN = Symbol();
export const REGISTER = Symbol();

export const USERS = Symbol();

export const ROUTES = {
  [LOGIN]: "/login",
  [REGISTER]: "/register",
  [CATALOG]: "/catalog",

  [EDIT_SCREEN]: "/catalog/screens/:screenId",
  [SCREENS]: "/catalog/screens",

  [CREATE_PRODUCT]: "/catalog/create-product",
  [EDIT_PRODUCT]: "/catalog/products/:productId",
  [PRODUCTS]: "/catalog/products",
  [ACCOUNTING_GROUPS]: "/catalog/groups",
  [USERS]: "/users",
  [DASHBOARD]: "/",
};
