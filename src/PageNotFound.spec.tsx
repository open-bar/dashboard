import { describe, test } from "vitest";
import { asAVisitor } from "./tests/Visitor";

describe("Page not found", () => {
  test("404 error displayed in case of page not found", async () => {
    await asAVisitor()
      .visisiting("/this-page-does-not-exist")
      .isOnPageNotFound();
  });
});
