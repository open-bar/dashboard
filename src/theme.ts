import "@fontsource/lilita-one";
import "@fontsource-variable/raleway";

import { createTheme, darken, lighten } from "@mui/material";

const white = "#ffffff";
const mainColor = "#36478f";
const secondaryColor = "#1cafaf";
const primaryTextColor = "#111727";

export const theme = createTheme({
  typography: {
    fontFamily: "'Raleway Variable', sans-serif",
    h1: {
      fontFamily: "'Lilita One', system-ui",
      fontSize: "54pt",
    },
    h2: {
      fontSize: "24pt",
      marginBottom: "24px",
    },
    h3: {
      fontSize: "20px",
    },
    h6: {
      fontFamily: "'Lilita One', system-ui",
    },
    body2: {
      marginBottom: "16px",
    },
  },
  palette: {
    primary: {
      main: mainColor,
      light: lighten(mainColor, 0.25),
      dark: darken(mainColor, 0.25),
      contrastText: white,
    },
    secondary: {
      main: secondaryColor,
      light: lighten(secondaryColor, 0.25),
      dark: darken(secondaryColor, 0.25),
      contrastText: primaryTextColor,
    },
    divider: "#E0E0E0",
    text: {
      primary: primaryTextColor,
    },
    error: {
      main: "#ad1f1f",
    },
  },
});
