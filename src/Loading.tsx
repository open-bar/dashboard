import React from "react";
import { Box, Typography, styled } from "@mui/material";
import { Loader } from "./components/Loader";

const Wrapper = styled(Box)({
  height: "100%",
  display: "flex",
  flexDirection: "column",
  gap: "32px",
  alignItems: "center",
  justifyContent: "center",
});

export const Loading: React.FC = () => {
  return (
    <Wrapper>
      <Loader />
      <Typography variant="h3">Chargement en cours…</Typography>
    </Wrapper>
  );
};
