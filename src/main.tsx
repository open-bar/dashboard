import * as Sentry from "@sentry/react";
import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.tsx";
import { IdentityProvider } from "./domains/identity/index.ts";

const isProduction = import.meta.env.MODE === "production";

Sentry.init({
  dsn: "https://4a768cfc1d85f64fade5dc26902c9a3f@o4507151719727104.ingest.de.sentry.io/4507151722938448",
  integrations: [Sentry.browserTracingIntegration()],
  // Performance Monitoring
  tracesSampleRate: 1.0, //  Capture 100% of the transactions
  // Set 'tracePropagationTargets' to control for which URLs distributed tracing should be enabled
  tracePropagationTargets: ["localhost:5173", /^https:\/\/yourserver\.io\/api/],
  enabled: isProduction,
  enableTracing: isProduction,
});

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <IdentityProvider>
      <App />
    </IdentityProvider>
  </React.StrictMode>
);
