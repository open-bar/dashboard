import React from "react";
import {
  GlobalStyles,
  Interpolation,
  Theme,
  ThemeProvider,
} from "@mui/material";
import { theme } from "./theme";

import { WithRouterProvider } from "./WithRouterProvider";
import { SnackBarRenderer } from "./domains/common/snackbar/components/SnackbarRenderer";

const globalStyles: Interpolation<Theme> = {
  body: { margin: 0, padding: 0 },
  a: {
    color: "inherit",
    textDecoration: "none",
  },
  "a:visited": {
    color: "inherit",
    textDecoration: "none",
  },
};

interface Props {
  initialEntries?: Partial<Location>[];
}

const App: React.FC<Props> = ({ initialEntries }) => {
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyles styles={globalStyles} />
      <WithRouterProvider initialEntries={initialEntries} />
      <SnackBarRenderer />
    </ThemeProvider>
  );
};

export default App;
