import React, { useMemo } from "react";
import {
  RouterProvider,
  createBrowserRouter,
  createMemoryRouter,
} from "react-router-dom";
import { allRoutes } from "./router";

const mode = import.meta.env.MODE;
const isTestMode = mode === "test";

interface Props {
  initialEntries?: Partial<Location>[];
}

export const WithRouterProvider: React.FC<Props> = ({ initialEntries }) => {
  const router = useMemo(() => {
    return isTestMode
      ? createMemoryRouter(allRoutes, {
          initialEntries,
        })
      : createBrowserRouter(allRoutes);
  }, [initialEntries]);
  return <RouterProvider router={router} />;
};
