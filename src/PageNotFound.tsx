import { Box, Button, Container, Typography, styled } from "@mui/material";
import React from "react";
import image from "/404.gif?url";
import { Link } from "react-router-dom";
import FullPageLayout from "./FullPageLayout";

const Wrapper = styled(Container)({
  flex: 1,
  height: "100%",
  display: "flex",
  alignItems: "center",
  justifyContent: "space-around",
  flexWrap: "wrap-reverse",
  boxSizing: "border-box",
  gap: "0px",
});

const Image = styled("figure")({
  flex: "1 1 50%",
  minWidth: "360px",
  height: "50vh",
  backgroundImage: `url(${image})`,
  backgroundRepeat: "no-repeat",
  backgroundSize: "contain",
  backgroundPosition: "center",
  margin: 0,
  padding: 0,
});

const Content = styled(Box)({
  flex: "1 1 50%",
  minWidth: "360px",
  textAlign: "center",
});

const PageNotFound: React.FC = () => {
  return (
    <FullPageLayout>
      <Wrapper>
        <Image />
        <Content>
          <Typography variant="h1">404</Typography>
          <Typography variant="h2">Page non trouvée</Typography>
          <Typography>Cette page n'existe pas.</Typography>

          <Link to="/">
            <Button variant="contained" sx={{ marginTop: 2 }}>
              Retourner à l'accueil
            </Button>
          </Link>
        </Content>
      </Wrapper>
    </FullPageLayout>
  );
};

export default PageNotFound;
